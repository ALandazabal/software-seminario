<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>
<?php
/*
 * if you have http://www.cniska.net/yii-bootstrap (Yii-Bootstrap) extention you can use code:
 * $this->widget('bootstrap.widgets.TbTabs', array(
 *   'placement'   => 'left',
 *   'encodeLabel' => false,
 *   'tabs'        => $categories,
 *));
 */
?>

<div class="tabs-left">
    <ul id="translation_file_list" class="nav nav-tabs">
        <?php foreach ($categories as $key => $value): ?>
            <li class="<?php if (isset($value['active'])) echo 'active' ?>">
                <a href="#<?= $value['id'] ?>" data-toggle="tab"><?= $value['label'] ?> </a>
            </li>
        <?php endforeach ?>
    </ul>
</div>
<div class="tab-content">
  
    <?php 
                foreach ($categories as $key => $value): ?>
  
        <?php 
  
  Yii::app()->controller->renderPartial('TranslationManagement.views.translation_one', ['category' => $value, 'translated_languages' => $this->translated_languages]) ?>
    <?php endforeach ?>
</div>

<?php //Yii::app()->controller->renderPartial('TranslationManagement.views.translation_modal', ['translated_languages' => $this->translated_languages]) ?>
<div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form id="translate_form" method="post" action="<?php echo $baseUrl."/admin/sitesettings/updatelang"; ?>">
     <ul class="nav nav-tabs">
                <?php
                $count = 0;
                foreach ($translated_languages as $key => $language): ?>
                    <li class="<?php if ($count++ === 0) echo 'active' ?>">
                        <a href="#<?= $key ?>" data-toggle="tab"><?= $language ?> </a>
                    </li>
                <?php endforeach ?>
            </ul>
            <div class="tab-content">
                <?php
                $count = 0;
                foreach ($translated_languages as $key => $language): ?>
                    <div id="<?= $key ?>" class="tab-pane fade <?php if ($count++ === 0) echo 'active in' ?>">
                        <textarea id="translate_<?= $key ?>" name="translate_<?= $key ?>"></textarea>
                    </div>
                <?php endforeach ?>
            </div>
            <input type="hidden" name="file_name" id="file_name">
            <input type="hidden" name="key_name" id="key_name">

        </div>
        
      </div>
 <div class="modal-footer">
            <button class="btn" data-dismiss="modal" aria-hidden="true">Close</button>
           <button type="submit" class="btn">Save</button>
        </div>
    </div>
    </form>
  </div>
</div>