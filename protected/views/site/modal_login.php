	<div class="modal fade" id="login-modal" role="dialog">
		<div class="modal-dialog modal-dialog-width">
		<div class="login-modal-content col-xs-8 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		<div class="login-modal-header col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		<h2 class="login-header-text"><?php echo Yii::t('app','Login to'); ?><?php echo " ".Myclass::getSiteName(); ?></h2></h2>
		<button data-dismiss="modal" class="close login-close" type="button">×</button>
		<p class="login-sub-header-text"><?php echo Yii::t('app','Signup or login to explore the great things available near you'); ?></p>
		</div>
		
		<div class="login-line col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding"></div>
			
		<div class="login-content col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding ">
			<div class="login-box col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
				<?php $currentLang = Yii::app()->language;
				if ($currentLang=='en'){ ?>
					<div class="login-text-box col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
							
							 <?php 
						         $model=new LoginForm();
						         $socialLogin = Myclass::getsocialLoginDetails();
			                     /*$siteSettingsModel = Sitesettings::model()->findByAttributes(array('id'=>1));
			                     $socialLogin = json_decode($siteSettingsModel->socialLoginDetails, true);*/
			                   $form=$this->beginWidget('CActiveForm', array(
			                                  'id'=>'login-form',
			                                 'action'=>Yii::app()->request->baseUrl.'/login', 
			                                'enableAjaxValidation'=>true,
			                               'enableClientValidation'=>true,
			                           'clientOptions'=>array(
			                           		'validateOnSubmit'=>true,
			                           		'validateOnChange'=>false,
			                           		),
			                           		'htmlOptions' => array(
			                           				'onSubmit' => 'return validsigninfrm()',
			                           		),
			                           							)); ?>


							<?php echo $form->textField($model,'username',array('class'=>'popup-input', 'placeholder'=>Yii::t('app','Enter your email addreses'))); ?>
							<?php echo $form->error($model,'username'); ?>
							<?php echo $form->passwordField($model,'password',array('class'=>'popup-input', 'placeholder'=>Yii::t('app','Enter your password'))); ?>
							<?php echo $form->error($model,'password'); ?>
						
						
						<?php echo CHtml::submitButton(Yii::t('app','Login'), array('class'=>'btn vende_tus_cosas')); ?>

						
					
						<div class="remember-pwd col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
							<div class="checkbox remember-me-checkbox ">
								<label><input type="checkbox" class="remember-me-checkbox cust_checkbox" value="" name="sport[]" ><?php echo Yii::t('app','Remember me'); ?></label>

							</div>
							<span class="remember-div">l</span>
							<a href="#" data-toggle="modal" data-target="#forgot-password-modal" data-dismiss="modal" class="forgot-pwd"><?php echo Yii::t('app','Forgot Password ?'); ?></a>
						</div>	
					</div>
				<?php } else if ($currentLang=='es') {?>
					<div class="login-text-box col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
							
							 <?php 
						         $model=new LoginForm();
						         $socialLogin = Myclass::getsocialLoginDetails();
			                     /*$siteSettingsModel = Sitesettings::model()->findByAttributes(array('id'=>1));
			                     $socialLogin = json_decode($siteSettingsModel->socialLoginDetails, true);*/
			                   $form=$this->beginWidget('CActiveForm', array(
			                                  'id'=>'login-form',
			                                 'action'=>Yii::app()->request->baseUrl.'/login', 
			                                'enableAjaxValidation'=>true,
			                               'enableClientValidation'=>true,
			                           'clientOptions'=>array(
			                           		'validateOnSubmit'=>true,
			                           		'validateOnChange'=>false,
			                           		),
			                           		'htmlOptions' => array(
			                           				'onSubmit' => 'return validsigninfrm()',
			                           		),
			                           							)); ?>


							<?php echo $form->textField($model,'username',array('class'=>'popup-input', 'placeholder'=>Yii::t('app','Enter your email addreses'))); ?>
							<?php echo $form->error($model,'username'); ?>
							<?php echo $form->passwordField($model,'password',array('class'=>'popup-input', 'placeholder'=>Yii::t('app','Enter your password'))); ?>
							<?php echo $form->error($model,'password'); ?>
						
						
						<?php echo CHtml::submitButton(Yii::t('app','Login'), array('class'=>'btn vende_tus_cosas')); ?>
						
					
						<div class="remember-pwd col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
							<div class="checkbox remember-me-checkbox ">
								<label><input type="checkbox" class="remember-me-checkbox cust_checkbox" value="" name="sport[]" ><?php echo Yii::t('app','Remember me'); ?></label>
							</div>
							<span class="remember-div">l</span>
							<a href="#" data-toggle="modal" data-target="#forgot-password-modal" data-dismiss="modal" class="forgot-pwd"><?php echo Yii::t('app','Forgot Password ?'); ?></a>
						</div>	
					</div>
				<?php } ?>
			<?php $this->endWidget(); ?>
			</div>
		</div>
		<?php $lineMaring = "no-margin"; ?>
		<?php if($socialLogin['facebook']['status'] == 'enable' || $socialLogin['twitter']['status'] == 'enable'
		|| $socialLogin['google']['status'] == 'enable'){ ?>
		<div class="login-div-line col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="left-div-line"></div>
			<div class="right-div-line"></div>
			<span class="login-or"><?php echo Yii::t('app','or'); ?></span>
		</div>
		<div class="social-login col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		<?php if($socialLogin['facebook']['status'] == 'enable'){ ?>
			<div class="facebook-login">
				<a href='<?php echo Yii::app()->createAbsoluteUrl("/user/socialLogin/type/facebook"); ?>' title='Facebook'>
					<img src="<?php echo Yii::app()->createAbsoluteUrl("/images/design/facebook-login.png"); ?>" alt="Facebook">
				</a>
			</div>
			<?php } ?>
			<?php if($socialLogin['twitter']['status'] == 'enable'){ ?>
			<div class="twitter-login">
				<a href='<?php echo Yii::app()->createAbsoluteUrl("/user/socialLogin/type/twitter"); ?>' title='Twitter'>
					<img src="<?php echo Yii::app()->createAbsoluteUrl("/images/design/twitter-login.png"); ?>" alt="Twitter">
				</a>
			</div>
			<?php } ?>
			<?php if($socialLogin['google']['status'] == 'enable'){ ?>
			<div class="googleplus-login">
				<a href="<?php echo Yii::app()->createAbsoluteUrl("/user/socialLogin/type/google"); ?>" title='Google'>
					<img src="<?php echo Yii::app()->createAbsoluteUrl("/images/design/googleplus-login.png"); ?>" alt="Google">
				</a>
			</div>
			<?php } ?>
		</div>
		<?php $lineMaring = ""; ?>
		<?php } ?>
		<div class="login-line-2 col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding <?php echo $lineMaring; ?>"></div>
		<div class="new-signup col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		
		<span><?php echo Yii::t('app','Not a member yet ?'); ?></span><a class="signup-link" data-dismiss="modal" data-toggle="modal" data-target="#signup-modal" href="#signup-modal"><?php echo Yii::t('app','click here'); ?></a>
		</div>
		
		</div>
		</div>
		</div>
