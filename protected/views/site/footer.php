	<div class="footer">
			<div class="container">
			<div class="row">	
				<div class="col-xs-12 col-sm-6 col-md-6 seccion-logo-footer">
							<div class="logo-footer-ppal-cont">
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/logofooter.png'); ?>" alt="Banner" class="logo-footer-ppal" > 
							</div>	
				</div>
<!-- 				<div class="col-md-1 barra-footer-ppal">
						<span class="joysale-footer-vertical-line-ppal"></span>
					</div> -->
				<div class="col-xs-12 col-sm-6 col-md-6">
					<div class="col-xs-12 col-sm-3 col-md-3 seccion-menu-footer">
						<ul>
							<?php $footerLinks = Myclass::getFooterLinks(); 
								if (!empty($footerLinks)){ 
							?>
							<?php $currentLang = Yii::app()->language;
								foreach ($footerLinks as $footerKey => $footerLink){
									$pageLink = Yii::app()->createAbsoluteUrl('help/'.$footerLink->slug);
							?>
							<li>
								<?php	if($currentLang =='en'){ ?>
									<a class="submenuf" href="<?php echo $pageLink; ?>"><?php echo $footerLink->page; ?>
										
									</a>
									<?php	} else if ($currentLang =='es'){ ?>
									<a class="submenuf" href="<?php echo $pageLink; ?>"><?php echo $footerLink->page_es; ?></a>
									<?php } else if ($currentLang =='fr'){ ?>
										<a class="submenuf" href="<?php echo $pageLink; ?>"><?php echo $footerLink->page; ?></a>
									<?php } ?>
							</li>
							<?php
								}
								}
							?>
							<li><a class="submenuf" href="<?php echo $baseUrl."/site/CatsSubcats" ?>">	<?php echo Yii::t('app','All Category'); ?></a></li>
							<!-- <li><a class="submenuf" href="/blog">Blog</a></li>	 -->
										
						</ul>
					</div>
					<div class="col-xs-12 col-sm-4 col-md-4  seccion-footer-3">
						<div class="col-xs-12 col-md-12  stay-connect-footerppal">
									<?php	//$currentLang = Yii::app()->language;
										if(	$currentLang=='en'){
									?> <span class="joysale-app-head"><?php //echo $footerSettings['applinkheading']; ?> </span> <?php
										} else if(	$currentLang=='es'){
									?> <span class="joysale-app-head"><?php //echo $footerSettings['applinkheading_es']; ?> </span> <?php
										} else if ($currentLang=='fr'){
									?> <span class="joysale-app-head"><?php //echo $footerSettings['applinkheading']; ?> </span> <?php
										}
									?>

									<?php if(!empty($footerSettings['appLinks']) && count($footerSettings['appLinks']) > 0){ ?>
									<!--<div class="joysale-app-icon">
										<?php if(isset($footerSettings['appLinks']['ios'])){ ?>
										<a class="joysale-ios-app" href="<?php echo $footerSettings['appLinks']['ios']; ?>" target="_blank"><img src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/ios.png'); ?>" alt="<?php echo Yii::t('app','ios app'); ?>" data-toggle="tooltip" title="" data-original-title="<?php echo Yii::t('app','iOS app'); ?>"></a>

										<?php } if(isset($footerSettings['appLinks']['ios']) && isset($footerSettings['appLinks']['android']) ){?>
										<span class="joysale-footer-vertical-line"></span>
										<?php } if(isset($footerSettings['appLinks']['android'])){ ?>
										<a href="<?php echo $footerSettings['appLinks']['android']; ?>" target="_blank" class="joysale-android-app"><img src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/android.png'); ?>" alt="<?php echo Yii::t('app','android app'); ?>" data-toggle="tooltip" title="" data-original-title="<?php echo Yii::t('app','Android app'); ?>"></a>

										<?php } ?>								
									</div>	-->
									<?php }else{ ?>
									<div class="joysale-noapp-icon"><?php echo Yii::t('app','Yet no applinks are not updated.'); ?></div>
									<?php }?>		
						</div>
<!--DESCOMENTAR 29/07
						<div class="col-xs-12 col-md-12 stay-connect-footerppal">
										<div class="joysale-social-connect">
										<span class="joysale-social-head">
											<?php
											$currentLang = Yii::app()->language;
											if(	$currentLang=='en'){
												//echo $footerSettings['socialloginheading']; 
											} else if(	$currentLang=='es'){
												//echo $footerSettings['socialloginheading_es']; 
											} else if ($currentLang=='fr'){
												//echo $footerSettings['socialloginheading']; 
											}
											?>
										</span>

										<?php if(!empty($footerSettings['socialLinks']) && count($footerSettings['socialLinks']) > 0){ ?>
											<!--
											<div class="joysale-social-icon">
												<div class="iconosRedes">
													<?php if(isset($footerSettings['socialLinks']['facebook'])){ ?>	
													<a href="<?php echo $footerSettings['socialLinks']['facebook']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/facebook.png'); ?>" alt="facebook"></a>
													<?php }if(isset($footerSettings['socialLinks']['twitter'])){ ?>
													<a href="<?php echo $footerSettings['socialLinks']['twitter']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/twitter.png'); ?>" alt="twitter"></a>
													<?php }if(isset($footerSettings['socialLinks']['google'])){ ?>
													<a href="<?php echo $footerSettings['socialLinks']['google']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/google-plus.png'); ?>" alt="google plus"></a>
												</div>
												<div class="iconosRedes">
												 <?php }if(isset($footerSettings['socialLinks']['instagram'])){ ?>
													<a href="<?php echo $footerSettings['socialLinks']['instagram']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/instagram.png'); ?>" alt="instagram"></a>
													 <?php }if(isset($footerSettings['socialLinks']['linkedin'])){ ?>
													<a href="<?php echo $footerSettings['socialLinks']['linkedin']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/linkedin.png'); ?>" alt="linked in"></a>
													 <?php }if(isset($footerSettings['socialLinks']['pinterest'])){ ?>
													<a href="<?php echo $footerSettings['socialLinks']['pinterest']; ?>" target="_blank"><img  class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/pinterest.png'); ?>" alt="pinterest"></a>
													<?php }?>
													 
												</div>
											</div> -->
<!--DESCOMENTAR 29/07 SOLO ESTA
										<?php }else{ ?>
										<!--	<div class="joysale-nosocial-icon"><?php echo Yii::t('app','Yet no sociallinks are not updated.'); ?></div> -->
<!--DESCOMENTAR 29/07	SOLOESTA
										<?php } ?>
									</div>	
								</div>
-->
					</div>
					<div class="col-xs-12 col-sm-5 col-md-5 footer-seccion4">
						<div class="col-xs-12 col-sm-12 col-md-12">
									<?php if(empty(Yii::app()->user->id)) {?>				
							<div>
								<p class="info-new-account-footer-ppal">
											<?php	//$currentLang = Yii::app()->language;
										if(	$currentLang=='en'){
											 echo $footerSettings['generaltextguest']; 
										} else if(	$currentLang=='es'){
											 echo $footerSettings['generaltextguest_es']; 
										} else if ($currentLang=='fr'){
											echo $footerSettings['generaltextguest']; 
										}
									?>
								</p>
								<a href="<?php echo Yii::app()->createAbsoluteUrl('user/signup'); ?>" 
									class="btn-crear-cuenta-footer-ppal">
									<?php echo Yii::t('app','Create a account'); ?>
								</a>							
							</div>		
						<?php }else{ ?>
							<div>
								<p class="info-new-account-footer-ppal">
											<?php	//$currentLang = Yii::app()->language;
										if(	$currentLang=='en'){
											 echo $footerSettings['generaltextuser']; 
										} else if(	$currentLang=='es'){
											 echo $footerSettings['generaltextuser_es']; 
										} else if ($currentLang=='fr'){
											echo $footerSettings['generaltextuser'];
										}
									?>
								</p>
								<a href="<?php echo Yii::app()->createAbsoluteUrl(
										'user/profiles',array('id'=>Myclass::safe_b64encode(Yii::app()->user->id.'-'.rand(0,999)))); ?>" 
									class="btn-crear-cuenta-footer-ppal">
									<?php echo Yii::t('app','Promote your list'); ?>
								</a>
							
							</div>
						<?php }?>	
						</div>

						<div class="col-xs-12 col-sm-12 col-md-12"> <!-- ///Ópcion para cambio de lenguaje-->

								<div class="language">									
											<?php #$this->widget('Language'); ?>
								</div>

						</div>
						
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-xs-12 col-sm-12 col-md-12">	
						<div class="joysale-footer-Copyright ">
							<?php if(!empty($footerSettings['footerCopyRightsDetails'])){
	
									if($currentLang =='en'){
										echo $footerSettings['footerCopyRightsDetails'];
									} else if ($currentLang =='es'){ 
										echo $footerSettings['footerCopyRightsDetails_es'];
									} else if ($currentLang =='fr'){ 
										echo $footerSettings['footerCopyRightsDetails'];
									} 
							}else{ ?>
							<span><?php echo Yii::t('app','© Copyright 2016 Hitasoft.com Limited. All rights reserved.'); ?> </span>
							<?php } ?>
						</div>
				</div>
			</div>

				
			</div>
			<div class="analytics-codes">
				<?php if(!empty($footerSettings['analytics'])){
					echo $footerSettings['analytics'];
				} ?>
			</div>
		</div>

