		<div class="container-fluid no-hor-padding">
			<?php $flashMessages = Yii::app()->user->getFlashes();
			if ($flashMessages) { 
				foreach($flashMessages as $key => $message) { ?>
			<div class=" flashes message-floating-div-cnt col-xs-12 col-sm-4 col-md-3 col-lg-3 no-hor-padding">
				<div class="flash-<?php echo $key; ?> floating-div no-hor-padding pull-right" style="width:auto;">
					<div class="message-user-info-cnt no-hor-padding" style="width:auto;">
						<div class="message-user-info"><?php echo $message; ?></div>
					</div>
				</div>
			</div>
			<?php } } ?>
		</div>	  