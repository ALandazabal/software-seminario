		<div id="page-content-wrapper pull-right open-overlay" class="rtl-menuicon-align">
			<a class="col-xs-1 col-sm-1 col-md-1 no-hor-padding" href="#menu-toggle" id="menu-toggle">
				<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/3-line.png'); ?>" alt="Menu">
			</a>
		</div>

		<div style="background-image: url('<?php echo Yii::app()->createAbsoluteUrl('/images/Banner.jpg'); ?>'); background-size: cover;background-repeat: no-repeat; height: 865px; background-position: bottom; "> 
		<div class="container-fluid header-container"  >
<!-- <img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Banner.jpg'); ?>" alt="Banner" style="opacity:0; width: 100%;">  -->
		<div class="row-fluid" >



		<?php if (empty(Yii::app()->user->id)){ ?>
				<div class="col-md-12 botones-grises2" >
						<!--<a class="boton-gris2"  href="#login-modal" data-toggle="modal" data-target="#login-modal" id="joysale-login"><?php #echo Yii::t('app','Login'); ?></a> 
							//Es este para el cambio del inicio de Sesión y Registrate-->
						<a class="boton-gris2 blanco"  href="#" data-toggle="modal" data-target="#login-modal"><?php echo Yii::t('app','Login'); ?><i class="fa fa-sign-in ico-sesion" aria-hidden="true"></i></a>
						<a class="boton-gris2 blanco" href="#" data-toggle="modal" data-target="#signup-modal"><?php echo Yii::t('app','Sign up'); ?><i class="fa fa-user-plus ico-sesion" aria-hidden="true"></i></a>

				</div>
	<?php } else {?>
<!-- 			aqui va las opciones una vez que el usuario esta logueado -->
<!-- 			hasta aqui va las opciones una vez que el usuario esta logueado -->
			<div class="col-xs-12 col-md-12 col-lg-12">
			<div class="menu-user-logued">
				<ul class="navbar-nav" style="display: inline-flex;">
								<li class="joysale-header-message-ppal">
									<a href="<?php echo Yii::app()->createAbsoluteUrl('message'); ?>">
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/message1.png'); ?>" alt="<?php echo Yii::t('app','Messages'); ?>" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="<?php echo Yii::t('app','Messages'); ?>" style="width:20px; height:20px;">
								<?php 
								$messageCount = Myclass::getMessageCount(Yii::app()->user->id); ?>
								<script>
									var liveCount = <?php echo $messageCount; ?>;
								</script>
								<?php 
								$messageStatus = "";
								if($messageCount == 0){
									$messageStatus = "message-hide";
								} 
								?>
								<span class="message-counter message-count <?php echo $messageStatus; ?>"><?php echo $messageCount; ?></span>
								
								</a>
	 							</li>
								
								<li class="joysale-header-message-ppal">
									<a href="<?php echo Yii::app()->createAbsoluteUrl('notification'); ?>">
										<img alt="<?php echo Yii::t('app','Notifications'); ?>" src="<?php echo Yii::app()->createAbsoluteUrl('/images/notification1.png'); ?>" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="<?php echo Yii::t('app','Notifications'); ?>" style="width:20px; height:20px;">
										<?php $notificationCount = Myclass::getNotificationCount(Yii::app()->user->id);
											$notificationStatus = "";
											if($notificationCount == 0 || Yii::app()->controller->action->id == 'notification'){
												$notificationStatus = "message-hide";
											} 
											?>
										<span class="message-counter <?php echo $notificationStatus; ?>"><?php echo $notificationCount; ?></span>
									</a>
								</li>
								
												
									<?php 
									$userImage = Myclass::getUserDetails(Yii::app()->user->id);
									if(!empty($userImage->userImage)) {
										$userimg = Yii::app()->createAbsoluteUrl('user/resized/35/'.$userImage->userImage);
										//echo CHtml::image(Yii::app()->createAbsoluteUrl('user/resized/35/'.$userImage->userImage),$userImage->username);
									} else {
										$userimg = Yii::app()->createAbsoluteUrl('user/resized/35/default/'.Myclass::getDefaultUser());
										//echo CHtml::image(Yii::app()->createAbsoluteUrl('user/resized/35/default/'.Myclass::getDefaultUser()),$userImage->username);
									}
									?>
									<li class="dropdown joysale-header-profile-ppal">
									  <a class="dropdown-toggle" data-toggle="dropdown" href="#">
									  <span class="joysale-header-profile-img-ppal img-responsive" style=" background: rgba(0, 0, 0, 0) url(<?php echo $userimg; ?>) no-repeat scroll 0 0 / cover ;"></span>
										<span class="joysale-header-down-arrow-ppal"></span>									  
									  </a>
									  <ul class="dropdown-menu dropdown-submenu">
										<li><a href="<?php echo Yii::app()->createAbsoluteUrl('user/profiles'); ?>"><?php echo Yii::t('app','Profile'); ?></a></li>	
<!-- 				Aqui va el link de promociones del menu profile 							 -->
<!-- 										<li><a href="<?php echo Yii::app()->createAbsoluteUrl('user/promotions',array(
												'id'=>Myclass::safe_b64encode(Yii::app()->user->id.'-'.rand(0,999)))); ?>"><?php echo Yii::t('app','My Promotions'); ?></a></li> -->
										<?php if($sitePaymentModes['exchangePaymentMode'] == 1){ ?>
										<li><a href="<?php echo Yii::app()->createAbsoluteUrl('item/exchanges',array('type' => 'incoming')); ?>"><?php echo Yii::t('app','My Exchanges'); ?></a></li>
										<?php } ?>
										<?php if($sitePaymentModes['buynowPaymentMode'] == 1){ ?>
										<li>
											<a href="<?php echo Yii::app()->createAbsoluteUrl('/orders'); ?>" class="joysale-exchange"><?php echo Yii::t('app','My Orders & My Sales'); ?></a>
										</li>
										<?php } ?>
										<!-- <li><a href="<?php echo Yii::app()->createAbsoluteUrl('orders'); ?>"><?php echo Yii::t('app','My Orders'); ?></a></li>
										<li><a href="<?php echo Yii::app()->createAbsoluteUrl('sales'); ?>"><?php echo Yii::t('app','My Sales'); ?></a></li>
										<li><a href="<?php echo Yii::app()->createAbsoluteUrl('coupons',array('type' => 'item')); ?>"><?php echo Yii::t('app','Coupons'); ?></a></li>	
										<li><a href="<?php echo Yii::app()->createAbsoluteUrl('shippingaddress'); ?>"><?php echo Yii::t('app','Shipping Addresses'); ?></a></li>
										<li><a href="#">Chat</a></li> -->
										<li class="logout"><a href="<?php echo Yii::app()->createAbsoluteUrl('user/logout'); ?>"><?php echo Yii::t('admin','Logout'); ?></a></li>
									  </ul>
									</li>									
<!-- 								<li class="joysale-header-stuff"><a class="joysale-camera-icon" href="<?php echo Yii::app()->createAbsoluteUrl('item/products/create'); ?>"><?php echo Yii::t('app','Sell your stuff'); ?></a></li> -->
					   </ul>
			</div>
			</div>
		<?php } ?>
		
		</div>
		<div class="row-fluid">
			<div class="col-md-12 seccion-vendetuscosas">
						<a class="vende_tus_cosas btn" href="<?php echo Yii::app()->createAbsoluteUrl('item/products/create'); ?>">
								<i class="fa fa-camera" aria-hidden="true" style="margin-right: 6px;
					vertical-align: sub; font-size: 16px;"></i> 
								<?php echo Yii::t('app','Sell your stuff'); ?>

						</a>	
			</div>
		</div>
		<div class="row-fluid">
			<div class="col-md-12 seccion-logo-ppal" >
				<a href="<?php Yii::app()->request->baseUrl;?>">
						<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/logo-sombra.png'); ?>" style="width:100%; max-width:423px;" alt="Banner" > 
				</a>
			</div>
		</div>
		<div class="row-fluid">
				<div class="col-md-12 seccion-categorias-principal">
			<!-- 	Menu dropdonw categorias principal				 -->
<!-- 					<div class="dropdown menu-categories-mobile">
						<button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown" style="background-color: transparent;">
						    <i class="fa fa-bars" aria-hidden="true"></i>	Category
								<span class="caret"></span>
						</button>
						<ul class="dropdown-menu ul-dropdown-categories">
							<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/forsale'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-For-Sale.png'); ?>" alt="" class="img-category-ppal"> FORSALE</a> </li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/housing'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Housing.png'); ?>" alt="" class="img-category-ppal"> HOUSING</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/vehicles'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Vehiculos-gris.png'); ?>" alt="" class="img-category-ppal"> VEHICLES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/jobs'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Jobs.png'); ?>" alt="" class="img-category-ppal"> JOBS</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/localservi'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Local-Services.png'); ?>" alt="" class="img-category-ppal"> LOCAL SERVICES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/coupons'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Coupons.png'); ?>" alt="" class="img-category-ppal"> COUPONS</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/exchanges'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icono-exchange.png'); ?>" alt="" class="img-category-ppal"> EXCHANGE</a></li>
						</ul>
 				 </div> -->
					
			<!-- 	Menu dropdonw categorias principal		 -->
					
<!-- 					menu principal viejo  -->
					
<!-- 					
					<?php 
						$currentLang = Yii::app()->language;
						if ($currentLang=='en'){ ?>
	 	   <ul class="lista-categoria-principal">
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/forsale'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-For-Sale.png'); ?>" alt="" class="img-category-ppal"> FORSALE</a> </li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/housing'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Housing.png'); ?>" alt="" class="img-category-ppal"> HOUSING</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/vehicles'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Vehiculos-gris.png'); ?>" alt="" class="img-category-ppal"> VEHICLES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/jobs'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Jobs.png'); ?>" alt="" class="img-category-ppal"> JOBS</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/localservi'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Local-Services.png'); ?>" alt="" class="img-category-ppal"> LOCAL SERVICES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/coupons'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Coupons.png'); ?>" alt="" class="img-category-ppal"> COUPONS</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/exchanges'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icono-exchange.png'); ?>" alt="" class="img-category-ppal"> EXCHANGE</a></li>
					</ul>
						<?php } else if ($currentLang=='es') {?>
				<ul class="lista-categoria-principal">
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/forsale'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-For-Sale.png'); ?>" alt="" class="img-category-ppal"> ARTÍCULOS EN VENTA</a> </li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/housing'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Housing.png'); ?>" alt="" class="img-category-ppal"> BIENES RAÍCES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/vehicles'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Vehiculos-gris.png'); ?>" alt="" class="img-category-ppal"> VEHÍCULOS</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/jobs'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Jobs.png'); ?>" alt="" class="img-category-ppal">EMPLEOS</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/localservi'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Local-Services.png'); ?>" alt="" class="img-category-ppal"> SERVICIOS LOCALES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/coupons'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Coupons.png'); ?>" alt="" class="img-category-ppal"> CUPONES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/exchanges'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icono-exchange.png'); ?>" alt="" class="img-category-ppal"> INTERCAMBIOS</a></li>
					</ul>
	   	 			<?php } else if ($currentLang=='fr') {?>
				<ul class="lista-categoria-principal">
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/forsale'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-For-Sale.png'); ?>" alt="" class="img-category-ppal"> FORSALE</a> </li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/housing'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Housing.png'); ?>" alt="" class="img-category-ppal"> HOUSING</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/vehicles'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Vehiculos-gris.png'); ?>" alt="" class="img-category-ppal"> VEHICLES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/jobs'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Jobs.png'); ?>" alt="" class="img-category-ppal"> JOBS</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/localservi'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Local-Services.png'); ?>" alt="" class="img-category-ppal"> LOCAL SERVICES</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/coupons'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Coupons.png'); ?>" alt="" class="img-category-ppal"> COUPONS</a></li>
						<li>
							<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/exchanges'); ?>"> 
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icono-exchange.png'); ?>" alt="" class="img-category-ppal"> EXCHANGE</a></li>
					</ul>
	   			 <?php } ?> -->
					
				<!-- 			hasta aqui		menu principal viejo  -->	
					
<!-- 					nueva implementacion del menu  -->
					<?php 
						$currentLang = Yii::app()->language;
						if ($currentLang=='en'){ ?>
						 <ul class="flex-container">
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/forsale'); ?>"> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-For-Sale.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' FORSALE'); ?></a> </li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/housing'); ?>"> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Housing.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' HOUSING'); ?> </a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/vehicles'); ?>"> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Vehiculos-gris.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' VEHICLES'); ?> </a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/jobs'); ?>"> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Jobs.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' JOBS'); ?> </a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/localservi'); ?>"> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Local-Services.png'); ?>" alt="" class="img-category-ppal"> <?php echo Yii::t('app', ' LOCAL SERVICES'); ?></a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/coupons'); ?>"> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Coupons.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' COUPONS'); ?> </a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/category/exchanges'); ?>"> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icono-exchange.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' EXCHANGE'); ?> </a></li>
						</ul>
					<?php } else if ($currentLang=='es') {?>
						<ul class="flex-container">
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/categoria/articulosenventa'); ?>"> 
									<meta name=”description” content="Venta de artículos nuevos y usados en Venezuela. Descubre la nueva forma de comprar. Herramientas, Celulares, Ropa, y más" />
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-For-Sale.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' ARTICULOS EN VENTA'); ?></a> </li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/categoria/bienesraices'); ?>">
									<meta name=”description” content="Venta y alquiler de ese espacio que tanto buscabas en Venezuela. Apartamentos, locales y casas, la forma más rápida y efectiva de encontrarlos sin salir de tu casa" /> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Housing.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' BIENES RAICES'); ?> </a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/categoria/vehiculos'); ?>">
									<meta name=”description” content="Venta de vehículos de transporte en Venezuela. Encuentra automóviles, motocicletas, botes, vehículos comerciales, camiones y más" /> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Vehiculos-gris.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' VEHICULOS'); ?> </a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/categoria/empleo'); ?>">
									<meta name=”description” content="Consigue las ofertas de empleo en Venezuela. Descubre clasificados de administración y oficina, servicios mecánicos, construcción, diseño, cosmetología y mas" />  
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Jobs.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' EMPLEO'); ?> </a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/categoria/servicioslocales'); ?>">
								<meta name=”description” content="Amplia variedad de servicios para ti en Venezuela. Encuentra los lugares que necesitas como; reparación de vehículos, guarderías, peluquerías, organización de eventos y mas" /> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Local-Services.png'); ?>" alt="" class="img-category-ppal"> <?php echo Yii::t('app', ' SERVICIOS LOCALES'); ?></a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/categoria/cupones'); ?>">
								<meta name=”description” content="Venta de cupones en Venezuela. Descubre las mejores rebajas y ofertas en ropa, artículos de limpieza, comida artículos electrónicos, muebles y más" /> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icon-Coupons.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' CUPONES'); ?> </a></li>
							<li class="flex-item">
								<a href="<?php echo Yii::app()->createAbsoluteUrl('/categoria/intercambios'); ?>">
								<meta name=”description” content="Intercambio de articulos nuevos y usados en Venezuela. Descubre la nueva forma de intercambiar. Herramientas, Celulares, Ropa, y más" /> 
									<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icono-exchange.png'); ?>" alt="" class="img-category-ppal"><?php echo Yii::t('app', ' INTERCAMBIOS'); ?> </a></li>
					<?php } ?>
<!-- 					hasta aqui nueva implementacion del menu  -->
					
					
				</div>
		</div>
		<div class="row-fluid">
			<?php $currentLang = Yii::app()->language;
			if ($currentLang=='en'){ ?>
				<div class="col-md-12 seccion-barra-busqueda-principal">
						<div class="col-sm-6 col-md-6 bloque-buscar">
							<form role="form" id="searchn" onSubmit="return dosearch();" class="navbar-form- navbar-left- search-form" style="padding-left: 0;" action="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>" method="get">
								<input  class="barra-buscar"  id="buscar-input" type="text" maxlength="30"  placeholder="<?php echo Yii::t('app','Search'); ?>"  class="joysale-search-icon form-control input-search <?php echo !empty(Yii::app()->user->id) ? "" : "sign" ?>" name="search">
								</input>
							</form>	
						</div>
						<div class="col-sm-6 col-md-6 bloque-location">
							<input id="pac-input" type="text" placeholder="<?php echo Yii::t('admin', 'Location'); ?>" class="controls barra-buscar-location" autocomplete="off">
								
							</input>
							<?php
								$sitesetting = Myclass::getSitesettings();
								$searchList = $sitesetting->searchList;
								$searcharr = explode(",",$searchList);
								$searchType = $sitesetting->searchType;
								if(isset($userModel)){
									$searchType=$userModel->distance;
									if($searchType != 'mi'){
										$searchTypedisplay = 'km';
									}else{
										$searchTypedisplay = 'mi';
									}
								}else{
									if($searchType != 'miles'){
										$searchTypedisplay = 'km';
									}else{
										$searchTypedisplay = 'mi';
									}
								}
							?>
	<!-- 								<div class="map-select-box">							 
										<select id="select-mapdistance" class="select-box-arrow" >						
												<?php
														if (!isset($kilometer)){
																$kilometer=50;
																echo '<option value="'.$kilometer.'" selected> -- '.$searchTypedisplay.'</option>';
														}else{
																echo '<option value="'.$kilometer.'"> -- '.$searchTypedisplay.'</option>';

														}

														for($i=0;$i<count($searcharr);$i++)
														{
															echo '<option value="'.$searcharr[$i].'">'.$searcharr[$i].' '.$searchTypedisplay.'</option>';
														}
												?>					 
										</select>
									</div> 

							//Boton de buscar
							<a href="javascript:void(0);" id="boton_buscar" class="btn-location-submit-ppal" ><img src="<?php //echo //Yii::app()->createAbsoluteUrl('/images/Icono-Lupa.png'); ?>" alt="" class="icon-btn-buscar-ppal"/>-->
								<a href="javascript:void(0);"  id="boton_buscar" class="boton-buscar" >
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icono-Lupa.png'); ?>" alt="" class="icon-btn-buscar-ppal"/>
							</a>

					
					</div>
	<!-- 				<div class="barra-ppal-mobile">	
						<?php //require_once(Yii::app()->basePath . '/views/site/mobile_search_bar.php');			?>
					</div> -->
				</div>
			<?php }else if ($currentLang=='es'){?>
				<div class="col-md-12 seccion-barra-busqueda-principal">
					
						<div class="col-sm-6 col-md-6 bloque-buscar">
							<form role="form" id="searchn" onSubmit="return dosearch();" class="navbar-form- navbar-left- search-form" style="padding-left: 0;" action="<?php echo Yii::app()->createAbsoluteUrl('/'); ?>" method="get">
								<input  class="barra-buscar" id="buscar-input" type="text" maxlength="30"  placeholder="<?php echo Yii::t('app','Search'); ?>"  class="joysale-search-icon form-control input-search <?php echo !empty(Yii::app()->user->id) ? "" : "sign" ?>" name="search">
								</input>
						  	</form>	
						</div>
						<div class="col-sm-6 col-md-6 bloque-location">
									<input id="pac-input" type="text" placeholder="<?php echo Yii::t('app', 'Location'); ?>" class="controls barra-buscar-location" autocomplete="off"></input>
												
										<?php
											$sitesetting = Myclass::getSitesettings();
											$searchList = $sitesetting->searchList;
											$searcharr = explode(",",$searchList);
											$searchType = $sitesetting->searchType;
											if(isset($userModel)){
												$searchType=$userModel->distance;
												if($searchType != 'mi'){
													$searchTypedisplay = 'km';
												}else{
													$searchTypedisplay = 'mi';
												}
											}else{
												if($searchType != 'miles'){
													$searchTypedisplay = 'km';
												}else{
													$searchTypedisplay = 'mi';
												}
											}
										?>
	<!-- 								<div class="map-select-box">							 
										<select id="select-mapdistance" class="select-box-arrow" >						
												<?php
														if (!isset($kilometer)){
																$kilometer=50;
																echo '<option value="'.$kilometer.'" selected> -- '.$searchTypedisplay.'</option>';
														}else{
																echo '<option value="'.$kilometer.'"> -- '.$searchTypedisplay.'</option>';

														}

														for($i=0;$i<count($searcharr);$i++)
														{
															echo '<option value="'.$searcharr[$i].'">'.$searcharr[$i].' '.$searchTypedisplay.'</option>';
														}
												?>					 
										</select>
									</div> 

							//Boton buscar
							<a href="javascript:void(0);" id="boton_buscar" class="btn-location-submit-ppal" >-->

							<a href="javascript:void(0);" id="boton_buscar" class="boton-buscar" >
								<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/Icono-Lupa.png'); ?>" alt="" class="icon-btn-buscar-ppal"/>
							</a>

						</div>
					
	<!-- 				<div class="barra-ppal-mobile">	
						<?php //require_once(Yii::app()->basePath . '/views/site/mobile_search_bar.php');			?>
					</div> -->
				</div>
			<?php } ?>
		</div>
		<div class="row-fluid">
			<?php
				$currentLang = Yii::app()->language;
						if ($currentLang=='en'){ ?>
							<div class="col-md-12 seccion-texto-ppal">
								<p>Platform that connects you to the community in real time.</p>
								<p>Sell, buy, exchange and publish yours advertisement and</p>
								<p>make it reach thousands of users in seconds.</p>
							</div>
				<?php }else if ($currentLang=='es'){ ?>
							<div class="col-md-12 seccion-texto-ppal">
								<p> Plataforma que te conecta con la comunidad en tiempo real. </p>
								<p>Vende, compra, intercambia y publica tus anuncios para  </p>
								<p> que llegue a miles de usuarios en segundos.</p>
							</div>
				<?php } ?>
		</div>
		<div class="row-fluid">
			<?php 
			$currentLang = Yii::app()->language;
						if ($currentLang=='en'){ ?>
										<div class="col-md-12 seccion-btn-video-ppal">
										<a href="#" class="launch-modal" data-modal-id="modal-video">
														<span class=""><i class="fa fa-play"></i></span>
														<span class="video-link-text">How it Works</span>
													</a>
										</div>
						<?php }else if ($currentLang=='es'){?>
							<div class="col-md-12 seccion-btn-video-ppal">
										<a href="#" class="launch-modal" data-modal-id="modal-video-es">
														<span class=""><i class="fa fa-play"></i></span>
														<span class="video-link-text">¿Cómo Funciona?</span>
													</a>
										</div>
						<?php } ?>
		
		</div>
		<!--  <div class="row-fluid">
			<div class="col-md-12 seccion-btn-app-ppal">
			<?php $footerSettings//=Myclass::getFooterSettings();?>
				<a href="<?php //echo $footerSettings['appLinks']['ios']; ?>" target="_blank">
        				<img src="<?php //echo Yii::app()->createAbsoluteUrl('/images/appstore.png'); ?>" alt="<?php //echo Yii::t('app','android app'); ?>" class="img-app-ios-ppal" >
      	</a>
				 <a href="<?php //echo $footerSettings['appLinks']['android']; ?>" target="_blank">
        				<img src="<?php //echo Yii::app()->createAbsoluteUrl('/images/google-play-badge.png'); ?>" alt="<?php // echo Yii::t('app','android app'); ?>" class="img-app-android-ppal" >
      	</a>
			</div>
		</div> -->
	</div>
	</div>
  <div class="modal fade" id="modal-video" tabindex="-1" role="dialog" aria-labelledby="modal-video-label">
        	<div class="modal-dialog" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        					<span aria-hidden="true">&times;</span>
        				</button>
        			</div>
        			<div class="modal-body">
        				<div class="modal-video">
	        				<div class="embed-responsive embed-responsive-16by9">
			                    <iframe width="560" height="315" src="https://www.youtube.com/embed/Qfzv7AsJyw0" frameborder="0" allowfullscreen></iframe>
			                </div>
		                </div>
        			</div>
        		</div>
        	</div>
        </div>
  <div class="modal fade" id="modal-video-es" tabindex="-1" role="dialog" aria-labelledby="modal-video-label">
        	<div class="modal-dialog" role="document">
        		<div class="modal-content">
        			<div class="modal-header">
        				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
        					<span aria-hidden="true">&times;</span>
        				</button>
        			</div>
        			<div class="modal-body">
        				<div class="modal-video">
	        				<div class="embed-responsive embed-responsive-16by9">
			                    <iframe width="560" height="315" src="https://www.youtube.com/embed/2ZCzKgTEtcI" frameborder="0" allowfullscreen></iframe>
			                </div>
		                </div>
        			</div>
        		</div>
        	</div>
        </div>
<script>
	 jQuery(document).ready(function() {
    
    /*
        Background slideshow
    */
//     $('.top-content').backstretch("assets/img/backgrounds/1.jpg");
    
    /*
        Modals
    */
    $('.launch-modal').on('click', function(e){
        e.preventDefault();
        $( '#' + $(this).data('modal-id') ).modal();
    });
    
});
</script>
