<div class="btn-app-div">
  <div class="btn-group-app">
    <div class="btn-app btn-app-google">
      <a href="<?php echo $footerSettings['appLinks']['android']; ?>" target="_blank">
        <img src="<?php echo Yii::app()->createAbsoluteUrl('/images/google-play-badge.png'); ?>" alt="<?php echo Yii::t('app','android app'); ?>" >
      </a>
       <a href="<?php echo $footerSettings['appLinks']['ios']; ?>" target="_blank">
        <img src="<?php echo Yii::app()->createAbsoluteUrl('/images/appstore.png'); ?>" alt="<?php echo Yii::t('app','android app'); ?>" >
      </a>
    </div>
<!--     <div class="btn-app btn-app-apple">
     
    </div> -->
  </div>
</div>