<div id="wrapper">        
		<div id="sidebar-wrapper">	
		
			<ul class="nav navbar-nav sidebar-nav">
			<?php  $categorypriority = Myclass::getCategoryPriority();?>
					<li class="joysale-mobile-Category">Category</li>
					<?php foreach($categorypriority as $key => $category): 
						if($category != "empty"){
							//$getcaname =  Myclass::getCatName($category);
							$getcatdet = Myclass::getCatDetails($category);
							$getcatimage = Myclass::getCatImage($category);
							$subCategory = Myclass::getSubCategory($category);
					
					?>
					<li class="dropdown">
					<a class="dropdown-toggle joysale-for-sale disabled" data-toggle="dropdown" href="<?php echo Yii::app()->createAbsoluteUrl('/category/'.$getcatdet->slug); ?>" style="background:url(<?php echo Yii::app()->createAbsoluteUrl('admin/categories/resized/70/'.$getcatimage); ?>) no-repeat scroll left center / 24px auto; " ><?php echo $getcatdet->name_es; ?></a>
						<?php if(!empty($subCategory)) {?>
						<ul  class="dropdown-menu joysale-dropdown-submenu">
							<?php foreach($subCategory as $key => $subCategory): 
							//echo $key;
									$subCatdet = Myclass::getCatDetails($key);
							?>
							<li><a href="<?php echo Yii::app()->createAbsoluteUrl('/category/'.$getcatdet->slug.'/'.$subCatdet->slug); ?>"><?php echo $subCategory; ?></a></li>
							<?php endforeach;?>
				  		</ul>
				  		<?php }?>
				  	</li>
					<?php } endforeach;?>	
			</ul>
			<?php if(!empty(Yii::app()->user->id)) {?>
			<div class="al-mobile-user-area">
				<a href="<?php echo Yii::app()->createAbsoluteUrl('item/products/create'); ?>" class="joysale-stuff-mob"><?php echo Yii::t('app','Sell your stuff'); ?></a>
				<a href="<?php echo Yii::app()->createAbsoluteUrl('user/profiles'); ?>" class="joysale-account"><?php echo Yii::t('app','Profile'); ?></a>
				<a href="<?php echo Yii::app()->createAbsoluteUrl('message'); ?>" class="joysale-account"><?php echo Yii::t('app','Message'); ?></a>
				<a href="<?php echo Yii::app()->createAbsoluteUrl('notification'); ?>" class="joysale-account"><?php echo Yii::t('app','Notifications'); ?></a>
				
				<?php if($sitePaymentModes['exchangePaymentMode'] == 1){ ?>
				<a href="<?php echo Yii::app()->createAbsoluteUrl('item/exchanges',array('type' => 'incoming')); ?>" class="joysale-exchange"><?php echo Yii::t('app','My Exchanges'); ?></a>				
				<?php } ?>
				
					<a href="<?php echo Yii::app()->createAbsoluteUrl('user/logout'); ?>" class="joysale-logout"><?php echo Yii::t('admin','Logout'); ?></a>
			</div>
			<?php }else {?>
			<div class="mobile-user-area">
				<a href="#" data-toggle="modal" data-target="#login-modal" class="joysale-login"><?php echo Yii::t('app','Login'); ?></a>
				<a href="#" data-toggle="modal" data-target="#signup-modal" class="joysale-signup"><?php echo Yii::t('app','Sign up'); ?></a>
			</div>
			<?php }?>
			
		</div> 
		
		
	</div>