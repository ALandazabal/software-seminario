	<div class="footer">
			<div class="container">
			
				<div class="row">
					<div class="joysale-footer-head col-xs-12 col-sm-12 col-md-12 col-lg-12">					
						<div class="joysale-social-connect col-xs-12 col-sm-6 col-md-3 col-lg-3 no-hor-padding">
							<span class="joysale-social-head">
						
								<?php
// 								print_r($footerSettings);exit();
								$currentLang = Yii::app()->language;
								if(	$currentLang=='en'){
									//echo $footerSettings['socialloginheading']; 
								} else if(	$currentLang=='es'){
									//echo $footerSettings['socialloginheading_es']; 
								} else if ($currentLang=='fr'){
									//echo $footerSettings['socialloginheading']; 
								}
								
								?>
							</span>
						
							<?php if(!empty($footerSettings['socialLinks']) && count($footerSettings['socialLinks']) > 0){ ?>
								<!--<div class="joysale-social-icon">
								  <?php if(isset($footerSettings['socialLinks']['facebook'])){ ?>	
									<a href="<?php echo $footerSettings['socialLinks']['facebook']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/facebook.png'); ?>" alt="facebook"></a>
								  <?php }if(isset($footerSettings['socialLinks']['twitter'])){ ?>
									<a href="<?php echo $footerSettings['socialLinks']['twitter']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/twitter.png'); ?>" alt="twitter"></a>
								  <?php }if(isset($footerSettings['socialLinks']['google'])){ ?>
									<a href="<?php echo $footerSettings['socialLinks']['google']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/google-plus.png'); ?>" alt="google plus"></a>
								 <?php }if(isset($footerSettings['socialLinks']['instagram'])){ ?>
									<a href="<?php echo $footerSettings['socialLinks']['instagram']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/instagram.png'); ?>" alt="instagram"></a>
									 <?php }if(isset($footerSettings['socialLinks']['linkedin'])){ ?>
									<a href="<?php echo $footerSettings['socialLinks']['linkedin']; ?>" target="_blank"><img class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/linkedin.png'); ?>" alt="linked in"></a>
									 <?php }if(isset($footerSettings['socialLinks']['pinterest'])){ ?>
									<a href="<?php echo $footerSettings['socialLinks']['pinterest']; ?>" target="_blank"><img  class="img-social-links-footer" src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/pinterest.png'); ?>" alt="pinterest"></a>
									<?php }?>
									 <a href="#"><img src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/instagram.png'); ?>" alt="instagram"></a>
								</div> -->
							<?php }else{ ?>
								<!-- <div class="joysale-nosocial-icon"><?php echo Yii::t('app','Yet no sociallinks are not updated.'); ?></div> -->
							<?php } ?>
						</div>						
						
						<div class="joysale-app-links col-xs-12 col-sm-6 col-md-2 col-lg-2 no-hor-padding">
							
							<?php	//$currentLang = Yii::app()->language;
								if(	$currentLang=='en'){
							?> <span class="joysale-app-head"><?php //echo $footerSettings['applinkheading']; ?> </span> <?php
								} else if(	$currentLang=='es'){
							?> <span class="joysale-app-head"><?php //echo $footerSettings['applinkheading_es']; ?> </span> <?php
								} else if ($currentLang=='fr'){
							?> <span class="joysale-app-head"><?php //echo $footerSettings['applinkheading']; ?> </span> <?php
								}
							
							?>
						
							<?php if(!empty($footerSettings['appLinks']) && count($footerSettings['appLinks']) > 0){ ?>
							<!-- <div class="joysale-app-icon">
							  <?php if(isset($footerSettings['appLinks']['ios'])){ ?>
								<a class="joysale-ios-app" href="<?php echo $footerSettings['appLinks']['ios']; ?>" target="_blank"><img src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/ios.png'); ?>" alt="<?php echo Yii::t('app','ios app'); ?>" data-toggle="tooltip" title="" data-original-title="<?php echo Yii::t('app','iOS app'); ?>"></a>
         
								<?php } if(isset($footerSettings['appLinks']['ios']) && isset($footerSettings['appLinks']['android']) ){?>
								<span class="joysale-footer-vertical-line"></span>
							  <?php } if(isset($footerSettings['appLinks']['android'])){ ?>
								<a href="<?php echo $footerSettings['appLinks']['android']; ?>" target="_blank" class="joysale-android-app"><img src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/android.png'); ?>" alt="<?php echo Yii::t('app','android app'); ?>" data-toggle="tooltip" title="" data-original-title="<?php echo Yii::t('app','Android app'); ?>"></a>
                
								<?php } ?>								
							</div>	-->
							<?php }else{ ?>
							<!-- <div class="joysale-noapp-icon"><?php echo Yii::t('app','Yet no applinks are not updated.'); ?></div> -->
							<?php }?>		
						</div>		
						<?php if(empty(Yii::app()->user->id)) {?>				
							<div class="joysale-new-account col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
								<p class="joysale-new-account-info col-xs-12 col-sm-9 col-md-9 col-lg-9 no-hor-padding">
									
											<?php	//$currentLang = Yii::app()->language;
										if(	$currentLang=='en'){
											 echo $footerSettings['generaltextguest']; 
										} else if(	$currentLang=='es'){
											 echo $footerSettings['generaltextguest_es']; 
										} else if ($currentLang=='fr'){
											echo $footerSettings['generaltextguest']; 
										}

									?>
								</p>
								
								<a href="<?php echo Yii::app()->createAbsoluteUrl('user/signup'); ?>" 
									class="joysale-create-btn col-xs-12 col-sm-3 col-md-3 col-lg-3 no-hor-padding">
									<?php echo Yii::t('app','Create a account'); ?>
								</a>
															
							</div>		
						<?php }else{ ?>
							<div class="joysale-new-account col-xs-12 col-sm-12 col-md-7 col-lg-7 no-hor-padding">
								<p class="joysale-new-account-info col-xs-12 col-sm-9 col-md-9 col-lg-9 no-hor-padding">
											<?php	//$currentLang = Yii::app()->language;
										if(	$currentLang=='en'){
											 echo $footerSettings['generaltextuser']; 
										} else if(	$currentLang=='es'){
											 echo $footerSettings['generaltextuser_es']; 
										} else if ($currentLang=='fr'){
											echo $footerSettings['generaltextuser'];
										}

									?>
								  
								</p>
									
								<a href="<?php echo Yii::app()->createAbsoluteUrl(
										'user/profiles',array('id'=>Myclass::safe_b64encode(Yii::app()->user->id.'-'.rand(0,999)))); ?>" 
									class="joysale-create-btn col-xs-12 col-sm-3 col-md-3 col-lg-3 no-hor-padding">
									<?php echo Yii::t('app','Promote your list'); ?>
								</a>
							
							</div>
						<?php }?>			
					</div>				
				</div>
				
				<div class="row">
					<div class="joysale-footer-horizontal-line col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding"></div>
					<div class="joysale-footer-bottom col-xs-12 col-sm-12 col-md-6 col-lg-8 no-hor-padding">
						<div class="joysale-footer-menu-links col-xs-12 col-sm-12 col-md-9 col-lg-9 no-hor-padding">
							<ul>
							<?php $footerLinks = Myclass::getFooterLinks(); 
								if (!empty($footerLinks)){ 
							?>
							<li>
							<?php 
									$currentLang = Yii::app()->language;
								foreach ($footerLinks as $footerKey => $footerLink){
								$pageLink = Yii::app()->createAbsoluteUrl('help/'.$footerLink->slug);
							?>
							<?php	if($currentLang =='en'){ ?>
								<a class="" href="<?php echo $pageLink; ?>"><?php echo $footerLink->page; ?></a>
							<?php	} else if ($currentLang =='es'){ ?>
								<a class="" href="<?php echo $pageLink; ?>"><?php echo $footerLink->page_es; ?></a>
								<?php } else if ($currentLang =='fr'){ ?>
								<a class="" href="<?php echo $pageLink; ?>"><?php echo $footerLink->page; ?></a>
								<?php } ?>
							</li>
							<?php if(count($footerLinks) > ($footerKey + 1)){ ?>
							<li class="joysale-footer-dev"><?php echo Yii::t('app','l'); ?></li>
							<?php 
									}	
								}
							?>
							
							<?php }?>
								<li class="joysale-footer-dev"><?php echo Yii::t('app','l'); ?></li>
								<a href="<?php echo $baseUrl."/site/CatsSubcats" ?>">
									All Category
								</a>						
							<!--<li><a href="#">Contact</a></li>
							<li class="joysale-footer-dev">l</li>								
							<li><a href="#">Terms of sales</a></li>
							<li class="joysale-footer-dev">l</li>	
							<li><a href="#">Terms of Services</a></li>
							<li class="joysale-footer-dev">l</li>	
							<li><a href="#">Privacy policy </a></li>
							<li class="joysale-footer-dev">l</li>	
							<li><a href="#">Terms and conditions</a></li>-->								
							</ul>
						</div>				
								
						
						<div class="joysale-footer-Copyright col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
							<?php if(!empty($footerSettings['footerCopyRightsDetails'])){
	
									if($currentLang =='en'){
										echo $footerSettings['footerCopyRightsDetails'];
									} else if ($currentLang =='es'){ 
										echo $footerSettings['footerCopyRightsDetails_es'];
									} else if ($currentLang =='fr'){ 
										echo $footerSettings['footerCopyRightsDetails'];
									} 
							}else{ ?>
							<span><?php echo Yii::t('app','© Copyright 2016 Hitasoft.com Limited. All rights reserved.'); ?> </span>
							<?php } ?>
						</div>
						
						
					</div>
					
		
						<div class="products-share-footer col-xs-12 col-sm-12 col-md-4 col-lg-2 no-hor-padding">
						
										<div><?php echo Yii::t('app','Share buttons:')?></div>  
						    <!-- Facebook -->
							<a href="http://www.facebook.com/sharer.php?u=http://ofersale.com<?php echo Yii::app()->request->requestUri; ?>" target="_blank">
									<img src="https://simplesharebuttons.com/images/somacro/facebook.png" alt="Facebook" />

							</a>

							<!-- Google+ -->
							<a href="https://plus.google.com/share?url=http://ofersale.com<?php echo Yii::app()->request->requestUri; ?>" target="_blank">
									<img src="https://simplesharebuttons.com/images/somacro/google.png" alt="Google" />
							</a>

							<!-- LinkedIn -->
							<a href="http://www.linkedin.com/shareArticle?mini=true&amp;url=http://ofersale.com<?php echo Yii::app()->request->requestUri; ?>" target="_blank">
									<img src="https://simplesharebuttons.com/images/somacro/linkedin.png" alt="LinkedIn" />
							</a>

							<!-- Twitter -->
							<a href="https://twitter.com/share?url=http://ofersale.com<?php echo Yii::app()->request->requestUri; ?>;text=Ofersale;hashtags=Ofersale" target="_blank">
									<img src="https://simplesharebuttons.com/images/somacro/twitter.png" alt="Twitter" />
							</a>
							
							  <!-- Pinterest -->
    					<a href="javascript:void((function()%7Bvar%20e=document.createElement('script');e.setAttribute('type','text/javascript');e.setAttribute('charset','UTF-8');e.setAttribute('src','http://assets.pinterest.com/js/pinmarklet.js?r='+Math.random()*99999999);document.body.appendChild(e)%7D)());">
        						<img src="https://simplesharebuttons.com/images/somacro/pinterest.png" alt="Pinterest" />
							</a>
					</div>
					
					
					
					
					
					<div class="language col-xs-12 col-sm-12 col-md-4 col-lg-2 no-hor-padding">									
						<?php $this->widget('Language'); ?>
					</div>
				</div>
				
			</div>
			<div class="analytics-codes">
				<?php if(!empty($footerSettings['analytics'])){
					echo $footerSettings['analytics'];
				} ?>
			</div>
		</div>