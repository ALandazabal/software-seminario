<div class="joysale-header">
<div class="container">
<div class="row">
<div class="joysale-header-bar col-xs-12 col-sm-12 col-md-12 col-lg-12" >
			<div class="joysale-logo col-xs-4 col-sm-4 col-md-1 col-lg-3 no-hor-padding">
				<?php $logo = Myclass::getLogo();
			
				echo CHtml::link(CHtml::image(Yii::app()->createAbsoluteUrl('images/logofooter.png'),"Logo", 
				array('style'=>'')),Yii::app()->createAbsoluteUrl('/')); ?>
		  </div>
			<div class=" col-xs-6 col-sm-6 col-md-6 col-lg-6 no-hor-padding">
			<?php require_once(Yii::app()->basePath . '/views/site/search_bar.php');?>
		  </div>
	
			
	
			<div class="joysale-user-nav col-sm-3 col-md-3 col-lg-3 no-hor-padding">
<!-- 				<div class="col-md-12 botones-grises-menu2" >
						<i class="fa fa-facebook" aria-hidden="true"></i>

					</div> -->
				<?php if(empty(Yii::app()->user->id)){?>
					<div class="col-md-12 botones-grises2" >
						<a class="boton-gris2"  href="#login-modal" data-toggle="modal" data-target="#login-modal" id="joysale-login"><?php echo Yii::t('app','Login'); ?></a>
						<a class="boton-gris2" href="#" data-toggle="modal" data-target="#signup-modal"><?php echo Yii::t('app','Sign up'); ?></a>

					</div>
				<?php }else {?>
						<div class="menu-user-logued">
									<ul class="navbar-nav">
													<li class="joysale-header-message-ppal">
														<a href="<?php echo Yii::app()->createAbsoluteUrl('message'); ?>">
														<img src="<?php echo Yii::app()->createAbsoluteUrl('/images/design/message.png'); ?>" alt="Message" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Message" style="width:20px; height:20px;">
													<?php 
													$messageCount = Myclass::getMessageCount(Yii::app()->user->id); ?>
													<script>
														var liveCount = <?php echo $messageCount; ?>;
													</script>
													<?php 
													$messageStatus = "";
													if($messageCount == 0){
														$messageStatus = "message-hide";
													} 
													?>
													<span class="message-counter message-count <?php echo $messageStatus; ?>"><?php echo $messageCount; ?></span>

													</a>
													</li>

													<li class="joysale-header-message-ppal">
														<a href="<?php echo Yii::app()->createAbsoluteUrl('notification'); ?>">
															<img alt="Notification" src="<?php echo Yii::app()->createAbsoluteUrl('/images/notification.png'); ?>" data-toggle="tooltip" title="" data-placement="bottom" data-original-title="Notification" style="width:20px; height:20px;">
															<?php $notificationCount = Myclass::getNotificationCount(Yii::app()->user->id);
																$notificationStatus = "";
																if($notificationCount == 0 || Yii::app()->controller->action->id == 'notification'){
																	$notificationStatus = "message-hide";
																} 
																?>
															<span class="message-counter <?php echo $notificationStatus; ?>"><?php echo $notificationCount; ?></span>
														</a>
													</li>


														<?php 
														$userImage = Myclass::getUserDetails(Yii::app()->user->id);
														if(!empty($userImage->userImage)) {
															$userimg = Yii::app()->createAbsoluteUrl('user/resized/35/'.$userImage->userImage);
															//echo CHtml::image(Yii::app()->createAbsoluteUrl('user/resized/35/'.$userImage->userImage),$userImage->username);
														} else {
															$userimg = Yii::app()->createAbsoluteUrl('user/resized/35/default/'.Myclass::getDefaultUser());
															//echo CHtml::image(Yii::app()->createAbsoluteUrl('user/resized/35/default/'.Myclass::getDefaultUser()),$userImage->username);
														}
														?>
														<li class="dropdown joysale-header-profile-ppal">
															<a class="dropdown-toggle" data-toggle="dropdown" href="#">
															<span class="joysale-header-profile-img-ppal img-responsive" style=" background: rgba(0, 0, 0, 0) url(<?php echo $userimg; ?>) no-repeat scroll 0 0 / cover ;"></span>
															<span class="joysale-header-down-arrow-ppal"></span>									  
															</a>
															<ul class="dropdown-menu dropdown-submenu">
															<li><a href="<?php echo Yii::app()->createAbsoluteUrl('user/profiles'); ?>"><?php echo Yii::t('app','Profile'); ?></a></li>	
					<!-- 				Aqui va el link de promociones del menu profile 							 -->
					<!-- 										<li><a href="<?php echo Yii::app()->createAbsoluteUrl('user/promotions',array(
																	'id'=>Myclass::safe_b64encode(Yii::app()->user->id.'-'.rand(0,999)))); ?>"><?php echo Yii::t('app','My Promotions'); ?></a></li> -->
															<?php if($sitePaymentModes['exchangePaymentMode'] == 1){ ?>
															<li><a href="<?php echo Yii::app()->createAbsoluteUrl('item/exchanges',array('type' => 'incoming')); ?>"><?php echo Yii::t('app','My Exchanges'); ?></a></li>
															<?php } ?>
															<?php if($sitePaymentModes['buynowPaymentMode'] == 1){ ?>
															<li>
																<a href="<?php echo Yii::app()->createAbsoluteUrl('/orders'); ?>" class="joysale-exchange"><?php echo Yii::t('app','My Orders & My Sales'); ?></a>
															</li>
															<?php } ?>
															<!-- <li><a href="<?php echo Yii::app()->createAbsoluteUrl('orders'); ?>"><?php echo Yii::t('app','My Orders'); ?></a></li>
															<li><a href="<?php echo Yii::app()->createAbsoluteUrl('sales'); ?>"><?php echo Yii::t('app','My Sales'); ?></a></li>
															<li><a href="<?php echo Yii::app()->createAbsoluteUrl('coupons',array('type' => 'item')); ?>"><?php echo Yii::t('app','Coupons'); ?></a></li>	
															<li><a href="<?php echo Yii::app()->createAbsoluteUrl('shippingaddress'); ?>"><?php echo Yii::t('app','Shipping Addresses'); ?></a></li>
															<li><a href="#">Chat</a></li> -->
															<li class="logout"><a href="<?php echo Yii::app()->createAbsoluteUrl('user/logout'); ?>"><?php echo Yii::t('admin','Logout'); ?></a></li>
															</ul>
														</li>									
					<!-- 								<li class="joysale-header-stuff"><a class="joysale-camera-icon" href="<?php echo Yii::app()->createAbsoluteUrl('item/products/create'); ?>"><?php echo Yii::t('app','Sell your stuff'); ?></a></li> -->
											 </ul>
			</div>
				<?php } ?>
				
					<div class="col-md-12 seccion-vendetuscosas2">
						<a class="vende_tus_cosas" href="<?php echo Yii::app()->createAbsoluteUrl('item/products/create'); ?>">
								<i class="fa fa-camera" aria-hidden="true" style="margin-right: 6px;
					vertical-align: sub; font-size: 27px;"></i> 
								<?php echo Yii::t('app','Sell your stuff'); ?>

						</a>	
			</div>
			
				</div>
				<!-- /#sidebar-wrapper --><!-- Mobile sidebar Content -->
<!-- 				<div id="page-content-wrapper pull-right open-overlay" class="rtl-menuicon-align">
				<a class="col-xs-1 col-sm-1 col-md-1 no-hor-padding" href="#menu-toggle" id="menu-toggle"><img src="<?php echo Yii::app()->createAbsoluteUrl('/images/3-line.png'); ?>" alt="Menu"></a>
		</div> -->


			
</div>
</div>
</div>
</div>