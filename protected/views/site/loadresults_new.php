
<?php $userInactivo=Myclass::getUserInactivo($product->userId); ?>
<?php foreach ($products as $product ): ?>
	<?php if (!$userInactivo): 
		$image = Myclass::getProductImage($product->productId);
		if(!empty($image)) {
				$img = $product->productId.'/'.$image;
				$img = Yii::app()->createAbsoluteUrl('media/item/'.$img);
			
				$imageSize = getimagesize($img);
				$imageWidth = $imageSize[0];
				$imageHeigth = $imageSize[1];
				if ($imageWidth > 300 && $imageHeigth > 300){
					$img = Yii::app()->createAbsoluteUrl("/item/products/resized/300/".$product->productId.'/'.$image);
				}
			} else {
				$img = 'default.jpeg';
				$img = Yii::app()->createAbsoluteUrl('media/item/'.$img);
			}
	?>

	
		<div class="col-xs-12 col-sm-6 col-md-4 col-lg-3" >
				<div class="item-new-ppal">
						<div class="item-img-product-ppal" style="background-image: url('<?=$img?>'); ">
						</div>
						<div class="item-name-product-ppal">
							<?=$product->name?>	
						</div>
						<div class="item-desc-product-ppal">
							<?=$product->description?>	
						</div>
						<div  class="item-location-product-ppal">
							<?=$product->location?>	
						</div>
						<div class="item-price-product-ppal">
							<?=$product->price?>	
							<?=$product->currency?>	
						</div>
				</div>
	</div>	
	<?php endif ?>

	
<?php endforeach ?>


<div class="row">
<div class="col-md-12 text-center">
	<div class="more-listing">
			<?php echo CHtml::ajaxLink('<img src="'.Yii::app()->createAbsoluteUrl('images/design/load-more.png').'" alt="img"><div class="list-text">'.Yii::t('app','More listing').'</div>', array('loadresults','search' => $search,'category'=>$category,'subcategory' => $subcategory,'lat' => $lat, 'lon' => $lon),
		array(
		'beforeSend'=> 'js:function(){$(".more-listing").hide();$(".joysale-loader").show();}',
		'data'=> 'js:{"limit": limit, "offset": offset, "loadData": 1,"adsOffset": adsoffset,"urgent": urgent,"ads": ads}',
		'success' => 'js:function(response){
		
					$(".more-listing").remove();

					$("#products").append(response);

					$(".joysale-loader").hide();
				
		 }',
		)
		); ?>
	</div>

		<div class="joysale-loader">
			<div class="cssload-loader"></div>
		</div>
	</div>	
	</div>
