	<!-- Modal content-->
	    <div id="nearmemodals" class="modal fade col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding" role="dialog" aria-hidden="true">
	       <div class="modal-dialog nearmemodal-content">		
			<div class="modal-content">
				<div class="modal-header">											
					<div class="location-section col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">	
						<button data-dismiss="modal" class="close map-close" type="button">×</button>					
						<div class="map-input-section col-xs-12 col-sm-12 col-md-12 col-lg-8 no-hor-padding">
							<div class="map-input-box">
							<!-- RAZ -->
								<input id="pac-input" type="text" placeholder="<?php echo Yii::t('admin', 'Where do you want to search?'); ?>" class="controls" autocomplete="off"></input>
						</div> <!-- map-input-box								 -->
							<?php
							$sitesetting = Myclass::getSitesettings();
							$searchList = $sitesetting->searchList;
							$searcharr = explode(",",$searchList);
							$searchType = $sitesetting->searchType;
							if($searchType != 'miles'){
								$searchTypedisplay = 'km';
							}else{
								$searchTypedisplay = 'mi';
							}
							?>
							<div class="map-select-box">							 
								<select id="select-mapdistance" class="select-box-arrow" >						
								<?php
								for($i=0;$i<count($searcharr);$i++)
								{
									if($kilometer == $searcharr[$i])
										echo '<option value="'.$searcharr[$i].'" selected>'.$searcharr[$i].' '.$searchTypedisplay.'</option>';
									else
										echo '<option value="'.$searcharr[$i].'">'.$searcharr[$i].' '.$searchTypedisplay.'</option>';
								}
?>					 
								</select>
							</div>
						
						</div>	<!-- map-input-section -->
							<div class="location-button col-xs-12 col-sm-12 col-md-12 col-lg-4 no-hor-padding">							
								<a href="javascript:void(0);" class="location-submit-button" onclick="return gotogetLocationData();"><?php echo Yii::t('admin', 'Submit'); ?></a>
								<a href="javascript:void(0);" class="location-find-button" onclick="removeLocation();"><?php echo Yii::t('admin', 'Remove'); ?></a>								
							</div>						
					</div>
					<!-- location-section -->
					<a href="javascript:void(0);" class="map-mylocation-button" data-toggle="tooltip" title="<?php echo Yii::t('app', 'Find my location!'); ?>" 
						onclick="getLatLong();">
						<img alt="find my location" src="<?php echo Yii::app()->createAbsoluteUrl('images/gps.png'); ?>">
					</a>		
					<div id="googleMap" class="google-Map col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
					</div>	
					</div>					<!-- modal-header						 -->
				</div>						<!-- modal-content							 -->
						  
			</div>			<!-- location-section -->
	    </div>	    <!-- end modal -->