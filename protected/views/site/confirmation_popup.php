<div class="modal fade" id="confirm_popup_container" role="dialog" aria-hidden="true"> 
			<div id="confirm-popup" class="modal-dialog modal-dialog-width confirm-popup">
				<div class="login-modal-content col-xs-8 col-sm-12 col-md-12 col-lg-12 no-hor-padding">	
					<div class="login-modal-header col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
							<h2 class="login-header-text"><?php echo Yii::t('app','Confirm'); ?></h2>
													
					</div>
						
					<div class="login-line col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding"></div>
				
					<div class="login-content col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding ">
						<span class="delete-sub-text col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
							<?php echo Yii::t('app','Are you sure you want to proceed ?'); ?>
						</span>
						<span class="confirm-btn">
							<a class="margin-bottom-0 post-btn" href="#" onclick="closeConfirm()">
								<?php echo Yii::t('app','ok'); ?>
							</a>
						</span>
						<a class="margin-bottom-0 delete-btn margin-10" href="#" onclick="closeConfirm()">
							<?php echo Yii::t('app','cancel'); ?>
						</a>			
					</div>			
				</div>
			</div>
		</div>