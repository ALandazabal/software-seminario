	<div class="modal fade" id="forgot-password-modal" role="dialog">
		<div class="modal-dialog modal-dialog-width">
		<div class="login-modal-content col-xs-8 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		<div class="login-modal-header col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		<h2 class="forgot-header-text"><?php echo Yii::t('app','Forgot Password'); ?></h2>
		<button data-dismiss="modal" class="close login-close" type="button">×</button>
		<p class="forgot-sub-header-text"><?php echo Yii::t('app',"Enter your email address and we will send you a link to reset your password."); ?></p>
					</div>
		
						<div class="forgot-line col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding"></div>
			
							<div class="forgot-content col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding ">
								<div class="forgot-box col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
									<div class="forgot-text-box col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
									<?php 
									$models = new Users('forgetpassword');
									$form = $this->beginWidget('CActiveForm', array(
										'id'=>'forgetpassword-form',
										'action'=>Yii::app()->createURL('/forgotpassword'),
										'enableAjaxValidation'=>true,
										'htmlOptions'=>array(
											'onsubmit'=>'return validforgot()',
									),
									)); ?>
									<?php echo $form->textField($models,'email',array('class' => 'forgetpasswords popup-input forget-input', 
											'placeholder'=>Yii::t('app','Enter your email address'))); ?>
									<?php echo $form->error($models,'emails'); ?>
									<?php echo CHtml::submitButton(Yii::t('app','Reset Password'), 
											array('class'=>'col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding forgot-btn','style'=>'margin-top:10px;')); ?>
									<?php $this->endWidget(); ?>
									</div>
								</div>
					
							</div>
		
			
		
			</div>
		</div>
	</div>