<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
	<?php $metaInformation = Myclass::getMetaData();//echo "<pre>";print_r($metaInformation); ?>
	<meta name="description"
		content="<?php echo isset($this->fbdescription) ? $this->fbdescription : $metaInformation['description']; ?>" />
	<meta name="keywords" content="<?php echo $metaInformation['metaKeywords']; ?>" />
	
	<meta name="language" content="en">
	
	<!-- For Facebook meta values -->
	<meta property="og:site_name" content="<?php echo $metaInformation['sitename']; ?>"/>
	<?php //if(isset($this->fbtitle)) { ?>
	<meta property="og:title" content="<?php echo isset($this->fbtitle) ? $this->fbtitle : $metaInformation['title']; ?>" />
	<?php //} ?>
	<meta property="og:type" content="products" />
	<meta property="og:url"
		content="<?php echo Yii::app()->request->hostInfo . Yii::app()->request->url; ?>" />
	<?php if(isset($this->fbimg)) { ?>
	<meta property="og:image" content="<?php echo $this->fbimg; ?>" />
	<meta name="twitter:image" content="<?php echo $this->fbimg; ?>">
	<meta itemprop="image" content="<?php echo $this->fbimg; ?>">
	<?php } //if(isset($this->fbdescription)) ?>
	<meta property="og:description"
		content="<?php echo isset($this->fbdescription) ? $this->fbdescription : $metaInformation['description']; ?>" />
		
	<!-- For Twitter meta values -->
	<meta name="twitter:title" content="<?php echo CHtml::encode($this->pageTitle); ?>">
	<meta name="twitter:description" content="<?php echo isset($this->fbdescription) ? $this->fbdescription : $metaInformation['description']; ?>">
	<meta name="twitter:card" content="summary">
	<meta name="twitter:site" content="<?php echo $metaInformation['sitename']; ?>">
	
	<!-- For Google+ meta values -->
	<meta itemprop="name" content="<?php echo $metaInformation['sitename']; ?>">
	<meta itemprop="description" content="<?php echo isset($this->fbdescription) ? $this->fbdescription : $metaInformation['description']; ?>">

<?php
if($this->uniqueid != "products" && $this->action->Id != "view"){
	echo '<link href="'.Yii::app()->request->hostInfo . Yii::app()->request->url.'" rel="canonical" />';
}

$baseUrl = Yii::app()->baseUrl;
Yii::app()->clientScript->registerScript('helpers','
  		yii = {
  		urls: {
		  	base: '.CJSON::encode(Yii::app()->baseUrl).'
		}
	  };',CClientScript::POS_HEAD);
Yii::app()->clientScript->registerCoreScript('jquery');
$cs = Yii::app()->getClientScript();
$cs->scriptMap=array(
		'jquery.js'=>false,
		'jquery.min.js'=>false,
		'jquery-ui.min.js'=>false
);
//Include to avoid exception in jquery
$cs->coreScriptPosition = $cs::POS_END;


//Yii::app()->clientScript->registerCoreScript('jquery.ui');
//design integration
$cs->registerScriptFile($baseUrl.'/js/design/salvattore.min.js');
//$cs->registerScriptFile($baseUrl.'/js/design/jquery.js');
$cs->registerScriptFile($baseUrl.'/js/design/jquery.easing.1.3.js');
$cs->registerScriptFile($baseUrl.'/js/design/jquery.magnific-popup.min.js');
//$cs->registerScriptFile($baseUrl.'/js/design/jquery.min.js');
$cs->registerScriptFile($baseUrl.'/js/design/jquery.waypoints.min.js');
$cs->registerScriptFile($baseUrl.'/js/design/modernizr-2.6.2.min.js');
$cs->registerScriptFile($baseUrl.'/js/design/modernizr.js');
$cs->registerScriptFile($baseUrl.'/js/design/respond.min.js');


//$cs->registerScriptFile($baseUrl.'/js/design/jquery.js');
$cs->registerScriptFile($baseUrl.'/js/design/bootstrap.min.js');
//$cs->registerScriptFile($baseUrl.'/js/bootstrap.min.js');
$cs->registerScriptFile($baseUrl.'/js/front.js');
$cs->registerScriptFile($baseUrl.'/slick/slick.min.js');
	$cs->registerScriptFile($baseUrl.'/select2/js/select2.min.js');

//$cs->registerScriptFile($baseUrl.'/js/design/bootstrap.js');
//$cs->registerScriptFile($baseUrl.'/js/node_modules/socket.io/node_modules/socket.io-client/dist/socket.io.js');
//$cs->registerScriptFile($baseUrl.'/js/nodeClient.js');
//$cs->registerCssFile($baseUrl.'/css/bootstrap.min.css');
$cs->registerCssFile($baseUrl.'/css/design/bootstrap.min.css');
$cs->registerCssFile($baseUrl.'/css/form.css');
$cs->registerCssFile($baseUrl.'/slick/slick.css');
$cs->registerCssFile($baseUrl.'/slick/slick-theme.css');
$cs->registerCssFile($baseUrl.'/select2/css/select2.min.css');

// 	<link href="path/to/select2.min.css" rel="stylesheet" />
// <script src="path/to/select2.min.js"></script>
//$cs->registerCssFile($baseUrl.'/css/design/joysale-style.css');
//$cs->registerCssFile($baseUrl.'/css/design/animate.css');
$cs->registerCssFile($baseUrl.'/font-awesome-4.1.0/css/font-awesome.min.css');

//$cs->registerCssFile($baseUrl.'/css/main.css');


//$cs->registerScriptFile($baseUrl.'/js/design/maps.js');
//$cs->registerScriptFile($baseUrl.'/js/design/main.js');


//$cs->registerCssFile($baseUrl.'/css/design/bootstrap.css'); 
/*
$cs->registerCssFile($baseUrl.'/css/design/bootstrap.css.map');*/
//$cs->registerCssFile($baseUrl.'/css/design/bootstrap.min.css'); 
/*
$cs->registerCssFile($baseUrl.'/css/design/bootstrap.min.css.map');
$cs->registerCssFile($baseUrl.'/css/design/bootstrap.theme.css');
$cs->registerCssFile($baseUrl.'/css/design/bootstrap.theme.css.map');
$cs->registerCssFile($baseUrl.'/css/design/bootstrap.theme.min.css.map');
$cs->registerCssFile($baseUrl.'/css/design/bootstrap.theme.min.css');*/
//$cs->registerCssFile($baseUrl.'/css/joysale-style.css');
//$cs->registerCssFile($baseUrl.'/css/animate.css');
?>
 
<!-- <link rel="stylesheet" type="text/css"	href="<?php echo Yii::app()->request->baseUrl; ?>/css/screen.css" media="screen, projection">
<link rel="stylesheet" type="text/css"	href="<?php echo Yii::app()->request->baseUrl; ?>/css/print.css" media="print"> 
-->
<!-- blueprint CSS framework
<link rel="stylesheet" type="text/css"	href="<?php echo Yii::app()->request->baseUrl; ?>/css/form.css"> 
<link rel="stylesheet" type="text/css"	href="<?php echo Yii::app()->request->baseUrl; ?>/css/main.css"> 
<link rel="stylesheet" type="text/css"	href="<?php echo Yii::app()->request->baseUrl; ?>/css/bootstrap.min.css"> 
<link rel="stylesheet" type="text/css"	href="<?php echo Yii::app()->request->baseUrl; ?>/font-awesome-4.1.0/css/font-awesome.min.css"> 
 -->
<!--[if lt IE 8]>
	<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/css/ie.css" media="screen, projection">
<![endif]-->


<!-- Bootstrap -->
<?php if(Yii::app()->language=='ar'){?>
			<link href="<?php echo Yii::app()->createAbsoluteUrl('css/boostrap-rtl.css'); ?>" rel="stylesheet">
			<?php } else { ?>
<link href="<?php echo Yii::app()->createAbsoluteUrl('css/bootstrap.min.css'); ?>" rel="stylesheet">
<?php } ?>	

<!-- Joysale style -->
<!-- <link href="<?php echo Yii::app()->createAbsoluteUrl('css/joysale-style.css'); ?>" rel="stylesheet">
<?php if(Yii::app()->language=='ar'){?>
<link href="<?php echo Yii::app()->createAbsoluteUrl('css/joysale-style_rtl.css'); ?>" rel="stylesheet">
<?php }?>

<!-- aqui van los estilos del slick -->
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/slick/slick.css"/>
<link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->request->baseUrl; ?>/slick/slick-theme.css"/>
<!-- hasta aqui van los estilos del slick -->

<link rel="icon" href="<?php echo Yii::app()->request->baseUrl; ?>/images/favicon.png">

<!-- <script	src="<?php echo Yii::app()->request->baseUrl; ?>/js/bootstrap.min.js"></script>
<script	src="<?php echo Yii::app()->request->baseUrl; ?>/js/front.js"></script> -->
<?php 
$siteSettings = Myclass::getSitesettings();
if(!empty($siteSettings) && isset($siteSettings->googleapikey) && $siteSettings->googleapikey!="")
$googleapikey = "&key=".$siteSettings->googleapikey;
else
$googleapikey = "";
?>
<script	src="<?php echo Yii::app()->request->baseUrl; ?>/js/design/jquery.js"></script> 
<script type="text/javascript" src="<?php echo Yii::app()->request->baseUrl; ?>/slick/slick.min.js"></script>
<script	src="https://maps.googleapis.com/maps/api/js?v=3.exp&libraries=places<?php echo $googleapikey;?>&language=<?php echo Yii::app()->language;?>"></script>
	
<title><?php echo CHtml::encode(isset($this->fbtitle) ? $metaInformation['sitename']." | ".$this->fbtitle : $metaInformation['sitename']." | ".$metaInformation['title']); ?></title>
</head>

<body>
<?php $footerSettings = Myclass::getFooterSettings();?>
<?php $logoDark = Myclass::getLogoDarkVersion(); ?>
<?php $sitePaymentModes = Myclass::getSitePaymentModes();
//echo "<pre>";print_r($sitePaymentModes); die;?>
<!-- <?php $variable = Yii::app()->createAbsoluteUrl('css/bootstrap.min.css'); ?> -->
	<!-- mobile Sidebar  -->
	<?php echo $this->render(Yii::app()->createAbsoluteUrl('protected/views/site/mobile_sidebar.php')); ?>	

	 
	<!-- E o mobile Sidebar -->
	
	<!--Header code-->
	<?php if(!empty(Yii::app()->user->id)) {?>
	
		<?php echo $this->render('header_logued'); ?>	


	
	<!--Mobile search bar code-->	
	
				 
	<?php echo $this->render('mobile_search_bar'); ?>
	<!--Mobile search bar code-->   


	<!--menu  logueado -->
<?php echo $this->render('menu_submenu'); ?>

	<!-- hasta aqui menu logueado -->
	
		
	
	
	<?php }else{ ?>
<?php echo $this->render('header_normal'); ?>

		
		<?php if((Yii::app()->controller->action->id != 'login') 
						 && (Yii::app()->controller->action->id != 'signup') &&
						 (Yii::app()->controller->action->id != 'socialLogin') &&
						 (Yii::app()->controller->action->id != 'forgotpassword'))
				{  ?>
		<!--Login modal-->
		
	<?php echo $this->render('modal_login'); ?>

		
		<!--E O Login modal-->
		
		<!--signup modal-->
		
				<?php echo $this->render('sign_up_modal'); ?>

		<!--E O signup modal-->
		<?php } ?>
		<?php if((Yii::app()->controller->action->id != 'signup') && (Yii::app()->controller->action->id != 'socialLogin') && (Yii::app()->controller->action->id != 'forgotpassword')){  ?>
		<!--Forgot password-->
			<?php echo $this->render('modal_forgot'); ?>
	
		
<!--E O Forgot password--->
	<?php } ?>	
		
	<!--Mobile search bar code-->
	<?php echo $this->render('mobile_search_bar'); ?>

	<!--Mobile search bar code-->

	<?php echo $this->render('menu_submenu'); ?>


	<?php }?>
			
		<!--Confirmation popup-->
		
				
			<?php echo $this->render('confirmation_popup'); ?>

	<?php echo $this->render('flash_Messages'); ?>




		<?php echo $content; ?>
		

		
		<?php echo $this->render('footer'); ?>

	
    <!-- Menu Toggle Script -->
    <script>
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
        $("#wrapper.toggled").css("display", "block");
        $("body").toggleClass("scroll-hidden");
        //$("#wrapper.toggled").parent("body").css('overflow':'hidden');
    });
    </script>
	<style>
	.scroll-hidden{
	overflow:hidden;
	}
	</style>

<!-- Sticky menu -->	
<script>	
$(document).ready(function(){
	$(window).scroll(function() {    
	    var scroll = $(window).scrollTop();
	    var headerHeightTrack = ($('.joysale-menu').height() - 64);

	    if (scroll >= headerHeightTrack) {
	        $(".joysale-header").addClass("affix");
	        $("#cont_bar").addClass("center_bar");
	    } else {
	        $(".joysale-header").removeClass("affix");
	        $("#cont_bar").removeClass("center_bar");
	    }
	});
});
</script>	


	
<!-- E O sticky menu -->

<!-- Tooltip menu -->	
<script>
$(document).ready(function(){
    $('[data-toggle="tooltip"]').tooltip();
});
</script>

<!-- E o tooltip menu -->
	
    <!-- Bootstrap -->
	<?php if(Yii::app()->language=='ar'){?>
				<link href="<?php echo Yii::app()->createAbsoluteUrl('css/boostrap-rtl.css'); ?>" rel="stylesheet">
				<?php } else { ?>
	<link href="<?php echo Yii::app()->createAbsoluteUrl('css/bootstrap.min.css'); ?>" rel="stylesheet">
	<?php } ?>
   <!-- <link href="<?php echo Yii::app()->createAbsoluteUrl('css/bootstrap.css'); ?>" rel="stylesheet">
	 E O Bootstrap -->	
	
	<!--Joysale style -->
	<link href="<?php echo Yii::app()->createAbsoluteUrl('css/joysale-style.css'); ?>" rel="stylesheet">
	<?php if(Yii::app()->language=='ar'){?>
				<link href="<?php echo Yii::app()->createAbsoluteUrl('css/joysale-style_rtl.css'); ?>" rel="stylesheet">
				<?php }?>
	<!--check box customization-->
	<script type="text/javascript" src="<?php echo Yii::app()->createAbsoluteUrl('js/design/check_script.js'); ?>"></script>
	<script type="text/javascript" src="<?php echo Yii::app()->createAbsoluteUrl('js/design/check_script_1.js'); ?>"></script>
	
		
		
	<!-- page -->
	<style type="text/css">
		.flashes{
			 -webkit-transition: all 3s ease-out;
		    -moz-transition: all 3s ease-out;
		    -ms-transition: all 3s ease-out;
		    -o-transition: all 3s ease-out;
		    transition: all 3s ease-out;
		    
		}
		.move{
			 
			 position: absolute;
		    -webkit-transition: all 3s ease-out;
		    -moz-transition: all 3s ease-out;
		    -ms-transition: all 3s ease-out;
		    -o-transition: all 3s ease-out;
		    transition: all 3s ease-out;
		    left: 200%;
		}
	</style>
	<script>

	$(document).keyup(function(e) {
		if (e.keyCode === 27){
	   		
	   		if ($('#login-modal').css('display') == 'block'){
		       $('#login-modal').modal('hide');
			}

	   		if ($('#signup-modal').css('display') == 'block'){
 			   $('#signup-modal').modal('hide');
			}
			
	   		if ($('#forgot-password-modal').css('display') == 'block'){
		       $('#forgot-password-modal').modal('hide');
			}

			if ($('#nearmemodals').css('display') == 'block'){
		       $('#nearmemodals').modal('hide');
			}

			if ($('#post-your-list').css('display') == 'block'){
		       $('#post-your-list').modal('hide');
			}

			if ($('#mobile-otp').css('display') == 'block'){
		       $('#mobile-otp').modal('hide');
			}

			if ($('#chat-with-seller-success-modal').css('display') == 'block'){
		       $('.modal').modal('hide');
		       $('#chat-with-seller-success-modal').css('display','none');
			}

			if ($('#offer-success-modal').css('display') == 'block'){
		       $('.modal').modal('hide');
		       $('#offer-success-modal').css('display','none');
			}
		}
	});

	var loginSession = readCookie('PHPSESSID');
	setTimeout(function() {
			//$(".flashes").slideRight();
			 //$('.flashes').toggle( "slide" );
			$(".flashes").addClass('move');
		}, 4000);
	function readCookie(name) {
	    var nameEQ = escape(name) + "=";
	    var ca = document.cookie.split(';');//console.log(document.cookie);
	    for (var i = 0; i < ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
	        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
	    }
	    return null;
	}
	if (typeof timerId != 'undefined'){
		clearInterval(timerId);
	}
	var timerId = setInterval(function() {
		var currentSession = readCookie('PHPSESSID');
	    if(loginSession != currentSession) {
		    //console.log('in reload '+loginSession+" "+currentSession);
		    window.location = '<?php echo Yii::app()->createAbsoluteUrl('/'); ?>';
		    clearInterval(timerId);
	        //Or whatever else you want!
	    }
	    
	},1000);
</script>
</body>
</html>

<style>
#language {
	float: left;
	//margin-right: 25px;
	margin-top: 3px;
	color: #2DAA98;
}
.lang-menu-front.pull-left {
    margin-top: 9px;
}

.affix >.container {
   	/*background:url("<?php echo Yii::app()->createAbsoluteUrl('/media/logo/'.$logoDark); ?>") no-repeat scroll left 15px;*/
}
</style>

