<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html>
<head>
   <script>
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
		(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
		m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
		ga('create', 'UA-93341050-1', 'auto');
		ga('send', 'pageview');
	</script>
    <meta name="google-site-verification" content="_ZEBTwsQSnFq8ATPcxGdIJNaE4PI7T3jvUDnHRsqmVk" />
 	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
		<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
					<?php $metaInformation = Myclass::getMetaData();//echo "<pre>";print_r($metaInformation); ?>
		<meta name="description"
					content="<?php echo isset($this->fbdescription) ? $this->fbdescription : $metaInformation['description']; ?>" />
		<meta name="keywords" content="<?php echo $metaInformation['metaKeywords']; ?>" />
		<meta name="language" content="en">
		<!-- For Facebook meta values -->
		<meta property="og:site_name" content="<?php echo $metaInformation['sitename']; ?>"/>
		<?php //if(isset($this->fbtitle)) { ?>
		<meta property="og:title" content="<?php echo isset($this->fbtitle) ? $this->fbtitle : $metaInformation['title']; ?>" />
		<?php //} ?>
		<meta property="og:type" content="products" />
		<meta property="og:url"
		content="<?php echo Yii::app()->request->hostInfo . Yii::app()->request->url; ?>" />
		<?php if(isset($this->fbimg)) { ?>
		<meta property="og:image" content="<?php echo $this->fbimg; ?>" />
		<meta name="twitter:image" content="<?php echo $this->fbimg; ?>">
		<meta itemprop="image" content="<?php echo $this->fbimg; ?>">
		<?php } //if(isset($this->fbdescription)) ?>
		<meta property="og:description"
		content="<?php echo isset($this->fbdescription) ? $this->fbdescription : $metaInformation['description']; ?>" />
		<!-- For Twitter meta values -->
		<meta name="twitter:title" content="<?php echo CHtml::encode($this->pageTitle); ?>">
		<meta name="twitter:description" content="<?php echo isset($this->fbdescription) ? $this->fbdescription : $metaInformation['description']; ?>">
		<meta name="twitter:card" content="summary">
		<meta name="twitter:site" content="<?php echo $metaInformation['sitename']; ?>">
		<!-- For Google+ meta values -->
		<meta itemprop="name" content="<?php echo $metaInformation['sitename']; ?>">
		<meta itemprop="description" content="<?php echo isset($this->fbdescription) ? $this->fbdescription : $metaInformation['description']; ?>">
		<!-- aqui van las llamadas ccs y javascript -->
		<?php require_once(Yii::app()->basePath . '/views/site/llamadas.php');?>
		<!--hasta aqui van las llamadas ccs y javascript -->
		<title><?php echo CHtml::encode(isset($this->fbtitle) ? $metaInformation['sitename']." | ".$this->fbtitle : $metaInformation['sitename']." | ".$metaInformation['title']); ?></title>

	</head>
	<script>
	$(window).load(function() {
		// Animate loader off screen
		$(".se-pre-con").fadeOut("slow");;
	});
	</script>
<body>
<!-- 	<div class="se-pre-con"></div> -->
<?php $footerSettings = Myclass::getFooterSettings();
	?>
<?php $logoDark = Myclass::getLogoDarkVersion(); ?>
<?php $sitePaymentModes = Myclass::getSitePaymentModes();
//echo "<pre>";print_r($sitePaymentModes); die;?>
<?php $variable = Yii::app()->createAbsoluteUrl('css/bootstrap.min.css'); ?>
	<!-- mobile Sidebar  -->
	<?php 
	
	require_once(Yii::app()->basePath . '/views/site/mobile_sidebar.php');

	?>	
	<!-- E o mobile Sidebar -->
	<!--Header code-->
	
<!-- 	aqui comienza la prueba -->
	
	<?php $urlo = Yii::app()->request->requestUri;
	require_once(Yii::app()->basePath . '/views/site/modal_login.php');
	require_once(Yii::app()->basePath . '/views/site/sign_up_modal.php');		
	require_once(Yii::app()->basePath . '/views/site/modal_forgot.php');	
	if($urlo == "/") {
	require_once(Yii::app()->basePath . '/views/site/homepage_site.php');
	}else{ ?>
	<?php if(!empty(Yii::app()->user->id)) {?>
	
			<?php 
				require_once(Yii::app()->basePath . '/views/site/header_normal.php');
			?>	
	<!--Mobile search bar code-->	 
		<?php
// 			require_once(Yii::app()->basePath . '/views/site/mobile_search_bar.php');			
		?>
	<!--Hasta aqui Mobile search bar code-->   
	<!--menu  logueado -->
		<?php 
				require_once(Yii::app()->basePath . '/views/site/menu_submenu.php');
		?>
	<!-- hasta aqui menu logueado -->

	<?php }else{ ?>

	<?php 
		require_once(Yii::app()->basePath . '/views/site/header_normal.php');	
	 ?>
		<?php if((Yii::app()->controller->action->id != 'login') 
						 && (Yii::app()->controller->action->id != 'signup') &&
						 (Yii::app()->controller->action->id != 'socialLogin') &&
						 (Yii::app()->controller->action->id != 'forgotpassword'))
				{  ?>
	<!--Login modal-->
<div class="modal fade" id="login-modal" role="dialog">
		<div class="modal-dialog modal-dialog-width">
		<div class="login-modal-content col-xs-8 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		<div class="login-modal-header col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		<h2 class="login-header-text"><?php echo Yii::t('app','Login too '); ?><?php echo Myclass::getSiteName(); ?></h2></h2>
		<button data-dismiss="modal" class="close login-close" type="button">×</button>
		<p class="login-sub-header-text"><?php echo Yii::t('app','Signup or login to explore the great things available near you'); ?></p>
		</div>
		
		<div class="login-line col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding"></div>
			
		<div class="login-content col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding ">
			<div class="login-box col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
				<div class="login-text-box col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
					
					 <?php 
				         $model=new LoginForm();
				         $socialLogin = Myclass::getsocialLoginDetails();
	                     /*$siteSettingsModel = Sitesettings::model()->findByAttributes(array('id'=>1));
	                     $socialLogin = json_decode($siteSettingsModel->socialLoginDetails, true);*/
	                   $form=$this->beginWidget('CActiveForm', array(
	                                  'id'=>'login-form',
	                                 'action'=>Yii::app()->request->baseUrl.'/login', 
	                                'enableAjaxValidation'=>true,
	                               'enableClientValidation'=>true,
	                           'clientOptions'=>array(
	                           		'validateOnSubmit'=>true,
	                           		'validateOnChange'=>false,
	                           		),
	                           		'htmlOptions' => array(
	                           				'onSubmit' => 'return validsigninfrm()',
	                           		),
	                           							)); ?>
		
					
					<?php echo $form->textField($model,'username',array('class'=>'popup-input', 'placeholder'=>Yii::t('app','Enter your email address'))); ?>
					<?php echo $form->error($model,'username'); ?>
					<?php echo $form->passwordField($model,'password',array('class'=>'popup-input', 'placeholder'=>Yii::t('app','Enter your password'))); ?>
					<?php echo $form->error($model,'password'); ?>
				
				
				<?php echo CHtml::submitButton(Yii::t('app','Login'), array('class'=>'col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding login-btn')); ?>
				
			
			<div class="remember-pwd col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
				<div class="checkbox remember-me-checkbox ">
					<label><input type="checkbox" class="remember-me-checkbox cust_checkbox" value="" name="sport[]" ><?php echo Yii::t('app','Remember me'); ?></label>
				</div>
				<span class="remember-div">l</span>
				<a href="#" data-toggle="modal" data-target="#forgot-password-modal" data-dismiss="modal" class="forgot-pwd"><?php echo Yii::t('app','Forgot Password ?'); ?></a>
			</div>	
			</div>
			<?php $this->endWidget(); ?>
			</div>
		</div>
		<?php $lineMaring = "no-margin"; ?>
		<?php if($socialLogin['facebook']['status'] == 'enable' || $socialLogin['twitter']['status'] == 'enable'
		|| $socialLogin['google']['status'] == 'enable'){ ?>
		<div class="login-div-line col-xs-12 col-sm-12 col-md-12 col-lg-12">
			<div class="left-div-line"></div>
			<div class="right-div-line"></div>
			<span class="login-or"><?php echo Yii::t('app','or'); ?></span>
		</div>
		<div class="social-login col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		<?php if($socialLogin['facebook']['status'] == 'enable'){ ?>
			<div class="facebook-login">
				<a href='<?php echo Yii::app()->createAbsoluteUrl("/user/socialLogin/type/facebook"); ?>' title='Facebook'>
					<img src="<?php echo Yii::app()->createAbsoluteUrl("/images/design/facebook-login.png"); ?>" alt="Facebook">
				</a>
			</div>
			<?php } ?>
			<?php if($socialLogin['twitter']['status'] == 'enable'){ ?>
			<div class="twitter-login">
				<a href='<?php echo Yii::app()->createAbsoluteUrl("/user/socialLogin/type/twitter"); ?>' title='Twitter'>
					<img src="<?php echo Yii::app()->createAbsoluteUrl("/images/design/twitter-login.png"); ?>" alt="Twitter">
				</a>
			</div>
			<?php } ?>
			<?php if($socialLogin['google']['status'] == 'enable'){ ?>
			<div class="googleplus-login">
				<a href="<?php echo Yii::app()->createAbsoluteUrl("/user/socialLogin/type/google"); ?>" title='Google'>
					<img src="<?php echo Yii::app()->createAbsoluteUrl("/images/design/googleplus-login.png"); ?>" alt="Google">
				</a>
			</div>
			<?php } ?>
		</div>
		<?php $lineMaring = ""; ?>
		<?php } ?>
		<div class="login-line-2 col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding <?php echo $lineMaring; ?>"></div>
		<div class="new-signup col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
		
		<span><?php echo Yii::t('app','Not a member yet ?'); ?></span><a class="signup-link" data-dismiss="modal" data-toggle="modal" data-target="#signup-modal" href="#signup-modal"><?php echo Yii::t('app','click here'); ?></a>
		</div>
		
		</div>
		</div>
		</div>
		
		<!--E O Login modal-->
		
	<?php 
			//require_once(Yii::app()->basePath . '/views/site/modal_login.php');
	?>
		<!--E O Login modal-->

		<!--signup modal-->
		
	<?php 
// 			require_once(Yii::app()->basePath . '/views/site/sign_up_modal.php');				
	?>
	<!--E O signup modal-->
	<?php } ?>
	<?php if((Yii::app()->controller->action->id != 'signup') && (Yii::app()->controller->action->id != 'socialLogin') && (Yii::app()->controller->action->id != 'forgotpassword')){  ?>
	<!--Forgot password-->
	<?php 
// 			require_once(Yii::app()->basePath . '/views/site/modal_forgot.php');		
			
	?>	
	<!--E O Forgot password-->
	<?php } ?>		
	<!--Mobile search bar code-->
	<?php 
			//require_once(Yii::app()->basePath . '/views/site/mobile_search_bar.php');
	?>
	<!--Mobile search bar code-->
	<?php 
			require_once(Yii::app()->basePath . '/views/site/menu_submenu.php');
	?>
	<?php }?>		
	
	<?php } ?>
	
<!-- 	hasta aqui la prueba  -->
	
	
		<!--Confirmation popup-->	
	<?php 
	require_once(Yii::app()->basePath . '/views/site/confirmation_popup.php');
	?>

	<?php 
			require_once(Yii::app()->basePath . '/views/site/flash_Messages.php');
	?>
	
	<?php echo $content; ?>
	<?php 
		require_once(Yii::app()->basePath . '/views/site/footer.php');
	?>
   <!-- Menu Toggle Script -->
    <script>
				$("#menu-toggle").click(function(e) {
						e.preventDefault();
						$("#wrapper").toggleClass("toggled");
						$("#wrapper.toggled").css("display", "block");
						$("body").toggleClass("scroll-hidden");
						//$("#wrapper.toggled").parent("body").css('overflow':'hidden');
				});
    </script>
	<style>
	.scroll-hidden{
	overflow:hidden;
	}
	</style>

<!-- Sticky menu -->	
<!-- <script>	
		$(document).ready(function(){
			$(window).scroll(function() {    
					var scroll = $(window).scrollTop();
					var headerHeightTrack = ($('.joysale-menu').height() - 64);

					if (scroll >= headerHeightTrack) {
							$(".joysale-header").addClass("affix");
							$("#cont_bar").addClass("center_bar");
					} else {
							$(".joysale-header").removeClass("affix");
							$("#cont_bar").removeClass("center_bar");
					}
			});
		});
</script>	 -->

<!-- E O sticky menu -->

<!-- Tooltip menu -->	
<script>
		$(document).ready(function(){
				$('[data-toggle="tooltip"]').tooltip();
		});
</script>

	
		
		
	<!-- page -->
	<style type="text/css">
		.flashes{
			 -webkit-transition: all 3s ease-out;
		    -moz-transition: all 3s ease-out;
		    -ms-transition: all 3s ease-out;
		    -o-transition: all 3s ease-out;
		    transition: all 3s ease-out;
		    
		}
		.move{
			 
			 position: absolute;
		    -webkit-transition: all 3s ease-out;
		    -moz-transition: all 3s ease-out;
		    -ms-transition: all 3s ease-out;
		    -o-transition: all 3s ease-out;
		    transition: all 3s ease-out;
		    left: 200%;
		}
	</style>
	<script>

	$(document).keyup(function(e) {
		if (e.keyCode === 27){
	   		
	   		if ($('#login-modal').css('display') == 'block'){
		      // $('#login-modal').modal('hide');
			}

	   		if ($('#signup-modal').css('display') == 'block'){
 			   $('#signup-modal').modal('hide');
			}
			
	   		if ($('#forgot-password-modal').css('display') == 'block'){
		       $('#forgot-password-modal').modal('hide');
			}

			if ($('#nearmemodals').css('display') == 'block'){
		       $('#nearmemodals').modal('hide');
			}

			if ($('#post-your-list').css('display') == 'block'){
		       $('#post-your-list').modal('hide');
			}

			if ($('#mobile-otp').css('display') == 'block'){
		       $('#mobile-otp').modal('hide');
			}

			if ($('#chat-with-seller-success-modal').css('display') == 'block'){
		       $('.modal').modal('hide');
		       $('#chat-with-seller-success-modal').css('display','none');
			}

			if ($('#offer-success-modal').css('display') == 'block'){
		       $('.modal').modal('hide');
		       $('#offer-success-modal').css('display','none');
			}
		}
	});

	var loginSession = readCookie('PHPSESSID');
	setTimeout(function() {
			//$(".flashes").slideRight();
			 //$('.flashes').toggle( "slide" );
			$(".flashes").addClass('move');
		}, 4000);
	function readCookie(name) {
	    var nameEQ = escape(name) + "=";
	    var ca = document.cookie.split(';');//console.log(document.cookie);
	    for (var i = 0; i < ca.length; i++) {
	        var c = ca[i];
	        while (c.charAt(0) === ' ') c = c.substring(1, c.length);
	        if (c.indexOf(nameEQ) === 0) return unescape(c.substring(nameEQ.length, c.length));
	    }
	    return null;
	}
	if (typeof timerId != 'undefined'){
		clearInterval(timerId);
	}
	var timerId = setInterval(function() {
		var currentSession = readCookie('PHPSESSID');
	    if(loginSession != currentSession) {
		    //console.log('in reload '+loginSession+" "+currentSession);
		    window.location = '<?php echo Yii::app()->createAbsoluteUrl('/'); ?>';
		    clearInterval(timerId);
	        //Or whatever else you want!
	    }
	    
	},1000);
</script>
</body>
</html>

<style>
#language {
	float: left;
	//margin-right: 25px;
	margin-top: 3px;
	color: #2DAA98;
	width:100%;/*colocado nuevo*/
}
.lang-menu-front.pull-left {
    margin-top: 9px;
}
.affix >.container {
   	/*background:url("<?php echo Yii::app()->createAbsoluteUrl('/media/logo/'.$logoDark); ?>") no-repeat scroll left 15px;*/
}
</style>

