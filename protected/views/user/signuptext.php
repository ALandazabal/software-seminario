<div class="slider container container-1 section_container">
	  <div class="row">
		<div class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
			  <!-- Bottom to top-->
			  <div class="row product_align_cnt">
				<div class="modal-dialog modal-dialog-width">
					<div class="login-modal-content col-xs-8 col-sm-12 col-md-12 col-lg-12 no-hor-padding">	
						<div class="login-modal-header col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding">
								<h2 class="login-header-text"> <?php echo Yii::t('app','Register in'); ?><?php echo Myclass::getSiteName(); ?></h2>
								<br><p class="login-sub-header-text"><?php echo Yii::t('app','Forgot password process has been completed'); ?></p>
						</div>
								<div class="login-content col-xs-12 col-sm-12 col-md-12 col-lg-12 no-hor-padding ">
					             	1. <?php echo Yii::t('app','In brief moments you will receive an email, with a link where you can continue with the process.'); ?> <br>
									2. <?php echo Yii::t('app','In case you do not receive the mail you should check the spam mailing list. '); ?> <br> 
				                   	3. <?php echo Yii::t('app','If you have not received an email, you can try again using the fallowing link: '); ?> <br>
				                   <br>
				                    <a class="forgot-pwd" href="#" data-toggle="modal" data-target="#signup-modal"><?php echo Yii::t('app','Resend link'); ?></a>
								</div>	


			
					</div>
				</div>	
			  </div>
			  <!-- end Bottom to top-->
		</div>
	</div>
</div>