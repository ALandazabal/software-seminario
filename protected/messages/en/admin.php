<?php
/**
 * Message translations.
 *
 * This file is automatically generated by 'yiic message' command.
 * It contains the localizable messages extracted from source code.
 * You may modify this file by translating the extracted messages.
 *
 * Each array element represents the translation (value) of a message (key).
 * If the value is empty, the message is considered as not translated.
 * Messages that no longer need translation will have their translations
 * enclosed between a pair of '@@' marks.
 *
 * Message string can be used with plural forms format. Check i18n section
 * of the guide for details.
 *
 * NOTE, this file must be saved in UTF-8 encoding.
 */
return array (
  'Dashboard' => 'Dashboard',
  'Manage' => '',
  'Add' => '',
  'Create' => 'Create',
  'Users' => 'Users',
  'Items' => 'Items',
  'Categories' => 'Categories',
  'User' => 'User',
  'Item' => 'Item',
  'Category' => 'Category',
  'Currency' => 'Currency',
  'Currency Management' => 'Currency Management',
  'Site Payment Options' => 'Site Payment Options',
  'Site Settings' => 'Site Settings',
  'Commission setup' => 'Commission setup',
  'Currencies' => 'Currencies',
  'Paypal Settings' => 'Paypal Settings',
  'Orders' => 'Orders',
  'Invoices' => 'Invoices',
  'Social Networks' => 'Social Networks',
  'Settings' => 'Settings',
  'Email' => 'Email',
  'Logout' => 'Logout',
  'Admin' => 'Admin',
  'Profile' => 'Profile',
  'Total' => 'Total',
  'View Details' => 'View Details',
  'Exchanges' => 'Exchanges',
  'Sales' => 'Sales',
  'Log' => 'Log',
  'Todays' => 'Todays',
  'Exchange To Buy' => 'Exchange To Buy',
  'Instant Buy' => 'Instant Buy',
  'Registered' => 'Registered',
  'Logged In' => 'Logged In',
  'Added' => 'Added',
  'Username' => 'Username',
  'Name' => 'Name',
  'Password' => 'Password',
  'Country' => 'Country',
  'City' => 'City',
  'State' => 'State',
  'Postalcode' => 'Postalcode',
  'User Image' => 'User Image',
  'Userstatus' => 'Userstatus',
  'Shipping Cost cannot be blank' => 'Shipping Cost cannot be blank',
  'Gender' => 'Gender',
  'Notification Settings' => 'Notification Settings',
  'Enable' => 'Enable',
  'Disable' => 'Disable',
  'Are you sure you want to enable this user?' => 'Are you sure you want to enable this user?',
  'Are you sure you want to disable this user?' => 'Are you sure you want to disable this user?',
  'Name is required' => 'Name is required',
  'Special Characters not allowed' => 'Special Characters not allowed',
  'Username is required' => 'Username is required',
  'Email is required' => 'Email is required',
  'Enter a valid email' => 'Enter a valid email',
  'Password should not be empty' => 'Password should not be empty',
  'Password must be greater than 5 characters long' => 'Password must be greater than 5 characters long',
  'Confirm Password should not be empty' => 'Confirm Password should not be empty',
  'Confirm Password must be greater than 5 characters long' => 'Confirm Password must be greater than 5 characters long',
  'Confirm password does not match' => 'Confirm password does not match',
  'Are you sure you want to change password?' => 'Are you sure you want to change password?',
  'Generate Password' => 'Generate Password',
  'Fields with' => 'Fields with',
  'are required' => 'are required',
  'List' => 'List',
  'View' => 'View',
  'Details' => 'Details',
  'User Created Successfully' => 'User Created Successfully',
  'User Details Updated Successfully' => 'User Details Updated Successfully',
  'Admin Profile Updated Successfully' => 'Admin Profile Updated Successfully',
  'User deleted Successfully' => 'User deleted Successfully',
  'Edit' => 'Edit',
  'Save' => 'Save',
  'Name cannot be blank' => 'Name cannot be blank',
  'Password cannot be blank' => 'Password cannot be blank',
  'Email cannot be blank' => 'Email cannot be blank',
  'Please Enter a valid Email' => 'Please Enter a valid Email',
  'Created Date' => 'Created Date',
  'Parent Category' => 'Parent Category',
  'Actions' => 'Actions',
  'Note: To create Parent Category, Leave this drop down Empty.' => 'Note: To create Parent Category, Leave this drop down Empty.',
  'Select Parent category' => 'Select Parent category',
  'Image cannot be blank' => 'Image cannot be blank',
  'Category created successfully.' => 'Category created successfully.',
  'Category updated successfully.' => 'Category updated successfully.',
  'One or more products has been added to this category.You cannot delete this category.' => 'One or more products has been added to this category.You cannot delete this category.',
  'Category Deleted Successfully' => 'Category Deleted Successfully',
  'Shortcode' => 'Shortcode',
  'Symbol' => 'Symbol',
  'Currency created successfully.' => 'Currency created successfully.',
  'Currency updated successfully.' => 'Currency updated successfully.',
  'Currency deleted successfully.' => 'Currency deleted successfully.',
  'Currency table should have atleast one record' => 'Currency table should have atleast one record',
  'Shortcode cannot be blank' => 'Shortcode cannot be blank',
  'Symbol cannot be blank' => 'Symbol cannot be blank',
  'Commission Amount In Percentage' => 'Commission Amount In Percentage',
  'Minimum Range' => 'Minimum Range',
  'Maximum Range' => 'Maximum Range',
  'PERCENTAGE' => 'PERCENTAGE',
  'MINIMUM RATE' => 'MINIMUM RATE',
  'MAXIMUM RATE' => 'MAXIMUM RATE',
  'DATE' => 'DATE',
  'STATUS' => 'STATUS',
  'ACTION' => 'ACTION',
  'Enabled' => 'Enabled',
  'Disabled' => 'Disabled',
  'Are you sure you want to delete?' => 'Are you sure you want to delete?',
  'Commission Amount cannot be blank' => 'Commission Amount cannot be blank',
  'Commission percentage should be between 1% to 100%.' => 'Commission percentage should be between 1% to 100%.',
  'Minimum Range cannot be blank' => 'Minimum Range cannot be blank',
  'Maximum Range cannot be blank' => 'Maximum Range cannot be blank',
  'Minimum Range should be greater than zero' => 'Minimum Range should be greater than zero',
  'Order ID' => 'Order ID',
  'Buyer' => 'Buyer',
  'Seller' => 'Seller',
  'Total Cost' => 'Total Cost',
  'Total Shipping' => 'Total Shipping',
  'Discount' => 'Discount',
  'Order Date' => 'Order Date',
  'All Orders' => 'All Orders',
  'Delivered' => 'Delivered',
  'Shipped' => 'Shipped',
  'New Orders' => 'New Orders',
  'Order' => 'Order',
  'Payment to' => 'Payment to',
  'Buyer Details' => 'Buyer Details',
  'Shipping Address' => 'Shipping Address',
  'Phone no' => 'Phone no',
  'Item Name' => 'Item Name',
  'Item Quantity' => 'Item Quantity',
  'Item Unitprice' => 'Item Unitprice',
  'Shipping fee' => 'Shipping fee',
  'Total Price' => 'Total Price',
  'Item Total' => 'Item Total',
  'Grand Total' => 'Grand Total',
  'Invoice ID' => 'Invoice ID',
  'Invoice Date' => 'Invoice Date',
  'Invoice Status' => 'Invoice Status',
  'Payment Method' => 'Payment Method',
  'Payment Status' => 'Payment Status',
  'Invoice' => 'Invoice',
  'on' => 'on',
  'Discount Amount' => 'Discount Amount',
  'SMTP settings updated successfully' => 'SMTP settings updated successfully',
  'Default' => 'Default',
  'Site Name' => 'Site Name',
  'Logo' => 'Logo',
  'Default User Image' => 'Default User Image',
  'Default settings updated successfully.' => 'Default settings updated successfully.',
  'Set Top' => 'Set Top',
  'Set' => 'Set',
  'Priority' => 'Priority',
  'Currency priority settings updated successfully' => 'Currency priority settings updated successfully',
  'Select Currency' => 'Select Currency',
  'cannot be blank' => 'cannot be blank',
  'should be unique' => 'should be unique',
  'Secret Key' => 'Secret Key',
  'Social Login' => 'Social Login',
  'Social settings updated successfully' => 'Social settings updated successfully',
  'Payment' => 'Payment',
  'User ID' => 'User ID',
  'Payment settings updated successfully' => 'Payment settings updated successfully',
  'Please Sign In' => 'Please Sign In',
  'Log In' => 'Log In',
  'Email Id or Password is incorrect.' => 'Email Id or Password is incorrect.',
  'Product ID' => 'Product ID',
  'Description' => 'Description',
  'Sub Category' => 'Sub Category',
  'Price' => 'Price',
  'Quantity' => 'Quantity',
  'Product Condition' => 'Product Condition',
  'Size Options' => 'Size Options',
  'Chat And Buy' => 'Chat And Buy',
  'Likes' => 'Likes',
  'Views' => 'Views',
  'Not Set' => 'Not Set',
  'Products' => 'Products',
  'Product' => 'Product',
  'Shipping Time' => 'Shipping Time',
  'Shipping Price' => 'Shipping Price',
  'Sold Item' => 'Sold Item',
  'Update' => 'Update',
  'Select Category' => 'Select Category',
  'Select subcategory' => 'Select subcategory',
  'More' => 'More',
  'Option' => 'Option',
  'Select a Country' => 'Select a Country',
  'Where the item is located?' => 'Where the item is located?',
  'Shipping Country' => 'Shipping Country',
  'Remove' => 'Remove',
  'Everywhere else' => 'Everywhere else',
  'Tell where you sell the item' => 'Tell where you sell the item',
  'Upload atleast a single product image' => 'Upload atleast a single product image',
  'Product Name cannot be blank' => 'Product Name cannot be blank',
  'Product Description cannot be blank' => 'Product Description cannot be blank',
  'Product Category cannot be blank' => 'Product Category cannot be blank',
  'Product Price cannot be blank' => 'Product Price cannot be blank',
  'Product Quantity cannot be blank' => 'Product Quantity cannot be blank',
  'Product Quantity cannot be blank or less than 1' => 'Product Quantity cannot be blank or less than 1',
  'Product Condition cannot be blank' => 'Product Condition cannot be blank',
  'Select a payment option' => 'Select a payment option',
  'Paypal ID cannot be blank' => 'Paypal ID cannot be blank',
  'Paypal ID should be a valid email id' => 'Paypal ID should be a valid email id',
  'Product Should have an shipping time' => 'Product Should have an shipping time',
  'Shipping Amount cannot be empty' => 'Shipping Amount cannot be empty',
  'Add atleast a single shipping' => 'Add atleast a single shipping',
  'Location Required' => 'Location Required',
  'Invalid Location.Select Location From Drop Down' => 'Invalid Location.Select Location From Drop Down',
  'Varient or Quantity or Price cannot be empty' => 'Varient or Quantity or Price cannot be empty',
  'Varient should not exceed 80 characters' => 'Varient should not exceed 80 characters',
  'Maximum Character limit Exceeded' => 'Maximum Character limit Exceeded',
  'Select Shipping Time' => 'Select Shipping Time',
  'PayPal ID cannot be Empty' => 'PayPal ID cannot be Empty',
  'Item created successfully.' => 'Item created successfully.',
  'Item updated successfully.' => 'Item updated successfully.',
  'Item deleted Successfully.' => 'Item deleted Successfully.',
  'Note: Select Location Only from Dropdown.Please dont enter manually' => 'Note: Select Location Only from Dropdown.Please dont enter manually',
  'Choose a approximate Shipping Time' => 'Choose a approximate Shipping Time',
  'Select Product Condition' => 'Select Product Condition',
  'Sell' => 'Sell',
  'Cancel' => 'Cancel',
  'Your changes will not be saved, Continue ?' => 'Your changes will not be saved, Continue ?',
  'Name should have minimum 4 characters' => 'Name should have minimum 4 characters',
  'Username should have minimum 4 characters' => 'Username should have minimum 4 characters',
  'Product Conditions' => 'Product Conditions',
  'Mobile Orders' => 'Mobile Orders',
  'Mobile and CC Orders' => 'Mobile and CC Orders',
  'API Credentials' => 'API Credentials',
  'Resend' => 'Resend',
  'Are you sure you want to resend the verification mail?' => 'Are you sure you want to resend the verification mail?',
  'Only numbers are allowed for quantity and price' => 'Only numbers are allowed for quantity and price',
  'Note: Select Location Only from Dropdown.Please dont enter manually.' => 'Note: Select Location Only from Dropdown.Please dont enter manually.',
  'Only numeric values allowed' => 'Only numeric values allowed',
  'Product condition has been updated Successfully' => 'Product condition has been updated Successfully',
  'Product condition has been created Successfully' => 'Product condition has been created Successfully',
  'Product Condition has been deleted Successfully' => 'Product Condition has been deleted Successfully',
  'Commission updated successfully' => 'Commission updated successfully',
  'Commission created successfully' => 'Commission created successfully',
  'Commission deleted successfully' => 'Commission deleted successfully',
  'Approve' => 'Approve',
  'Amount Paid' => 'Amount Paid',
  'Seller Amount' => 'Seller Amount',
  'Please wait…' => 'Please wait…',
  'Try again!' => 'Try again!',
  'Approved' => 'Approved',
  'Facebook Appid cannot be empty' => 'Facebook Appid cannot be empty',
  'Facebook Secret Key cannot be empty' => 'Facebook Secret Key cannot be empty',
  'Twitter Appid cannot be empty' => 'Twitter Appid cannot be empty',
  'Twitter Secret Key cannot be empty' => 'Twitter Secret Key cannot be empty',
  'Google Appid cannot be empty' => 'Google Appid cannot be empty',
  'Google Secret Key cannot be empty' => 'Google Secret Key cannot be empty',
  'API Settings' => 'API Settings',
  'API Username' => 'API Username',
  'API Password' => 'API Password',
  'Save as new' => 'Save as new',
  'Save & Make default' => 'Save & Make default',
  'Restore Default' => 'Restore Default',
  'Tracking Details' => 'Tracking Details',
  'Logistic Name' => 'Logistic Name',
  'Shipment Service' => 'Shipment Service',
  'Footer Facebook Link' => 'Footer Facebook Link',
  'Footer Google plus Link' => 'Footer Google plus Link',
  'Footer Twitter Link' => 'Footer Twitter Link',
  'Footer Android app Link' => 'Footer Android app Link',
  'Footer IOS app Link' => 'Footer IOS app Link',
  'Footer Settings' => 'Footer Settings',
  'Price should be greater than zero' => 'Price should be greater than zero',
  'This page will be linked in the Email Terms & Policy Link' => 'This page will be linked in the Email Terms & Policy Link',
  'Price and Quantity should be greater than zero' => 'Price and Quantity should be greater than zero',
  'Only images are allowed' => 'Only images are allowed',
  'Already Selected' => 'Already Selected',
  'Help Pages' => 'Help Pages',
  'New Comments!' => 'New Comments!',
  'New Tasks!' => 'New Tasks!',
  'Support Tickets!' => 'Support Tickets!',
  'Area Chart Example' => 'Area Chart Example',
  'Another action' => 'Another action',
  'Something else here' => 'Something else here',
  'Separated link' => 'Separated link',
  'Bar Chart Example' => 'Bar Chart Example',
  'Time' => 'Time',
  'Amount' => 'Amount',
  'Responsive Timeline' => 'Responsive Timeline',
  '11 hours ago via Twitter' => '11 hours ago via Twitter',
  'Notifications Panel' => 'Notifications Panel',
  '4 minutes ago' => '4 minutes ago',
  '3 New Followers' => '3 New Followers',
  'Message Sent' => 'Message Sent',
  '27 minutes ago' => '27 minutes ago',
  'New Task' => 'New Task',
  'New Comment' => 'New Comment',
  '43 minutes ago' => '43 minutes ago',
  'Server Rebooted' => 'Server Rebooted',
  'Server Crashed!' => 'Server Crashed!',
  'Server Not Responding' => 'Server Not Responding',
  'New Order Placed' => 'New Order Placed',
  'Payment Received' => 'Payment Received',
  'Yesterday' => 'Yesterday',
  'View All Alerts' => 'View All Alerts',
  'Donut Chart Example' => 'Donut Chart Example',
  'Chat' => 'Chat',
  'Refresh' => 'Refresh',
  'Available' => 'Available',
  'Busy' => 'Busy',
  'Away' => 'Away',
  'Sign Out' => 'Sign Out',
  '12 minutes ago' => '12 minutes ago',
  '13 minutes ago' => '13 minutes ago',
  '14 minutes ago' => '14 minutes ago',
  '15 minutes ago' => '15 minutes ago',
  'Send ; Envoyer' => 'Send ; Envoyer',
  'Now Following You' => 'Now Following You',
  'Hi' => 'Hi',
  'Stay Connected!' => 'Stay Connected!',
  'There is an alot of products and friends waiting for you' => 'There is an alot of products and friends waiting for you',
  'There is an offer placed in your account - The offer rate is' => 'There is an offer placed in your account - The offer rate is',
  'Congratulations!' => 'Congratulations!',
  'There is an offer placed in your profile. This email confirms that the same offer has beenprocessed and recommend you to proceed with the sales of this product' => 'There is an offer placed in your profile. This email confirms that the same offer has beenprocessed and recommend you to proceed with the sales of this product',
  'Offer Rate' => 'Offer Rate',
  'Date' => 'Date',
  'Phone' => 'Phone',
  'Message' => 'Message',
  'You have options to confirm this offer' => 'You have options to confirm this offer',
  'Welcome to Happy sale' => 'Welcome to Happy sale',
  'You have successfully created the account with us' => 'You have successfully created the account with us',
  'There is an order placed in your profile. This email confirms that' => 'There is an order placed in your profile. This email confirms that',
  'the same order has been processed and recommend you to proceed with the shipping of this order.' => 'the same order has been processed and recommend you to proceed with the shipping of this order.',
  'Ship to' => 'Ship to',
  'You have options to confirm this order and mark is as processing. You can also add the shipping details and tracking details to this order from â€œMy Salesâ€ under your profile account' => 'You have options to confirm this order and mark is as processing. You can also add the shipping details and tracking details to this order from â€œMy Salesâ€ under your profile account',
  'Thank you for shopping on' => 'Thank you for shopping on',
  'Kindly note down the order ID(s)' => 'Kindly note down the order ID(s)',
  'as a reference number for this purchase' => 'as a reference number for this purchase',
  'Your order is shipped. The same has been notified to the seller too. In case if you have ordered multiple items from multiple sellers; all your orders may be delivered separately. All your orders can be seen and tracked through your account also.' => 'Your order is shipped. The same has been notified to the seller too. In case if you have ordered multiple items from multiple sellers; all your orders may be delivered separately. All your orders can be seen and tracked through your account also.',
  'Your order details' => 'Your order details',
  'Order ID(s)' => 'Order ID(s)',
  'Subject' => 'Subject',
  'Shipped to' => 'Shipped to',
  'Your order is safe and been monitored closely with the seller when you shop on . In case if you donâ€™t receive your order from the seller(s) or it is delivered in an unsatisfactory condition; you can reach us through . When you write to us about any orders; please mention the order ID to get quick response from the support team.' => 'Your order is safe and been monitored closely with the seller when you shop on . In case if you donâ€™t receive your order from the seller(s) or it is delivered in an unsatisfactory condition; you can reach us through . When you write to us about any orders; please mention the order ID to get quick response from the support team.',
  'also recommended to update the system once you received the order from the seller as expected and you are satisfied. In case of any orderâ€™s receipt is not notified in the system will be automatically confirmed as Delivered and will processed in the favor of sellers. Within this duration you are recommended to reach' => 'also recommended to update the system once you received the order from the seller as expected and you are satisfied. In case of any orderâ€™s receipt is not notified in the system will be automatically confirmed as Delivered and will processed in the favor of sellers. Within this duration you are recommended to reach',
  'To update receipt of the order; please login to your account and go to profile and settings.' => 'To update receipt of the order; please login to your account and go to profile and settings.',
  'We look forward to see you again' => 'We look forward to see you again',
  'Greetings! Thanks for registering with' => 'Greetings! Thanks for registering with',
  'To complete the registration of your new account; please click the following link to verify this email address.' => 'To complete the registration of your new account; please click the following link to verify this email address.',
  'Click Here ; to confirm your registration' => 'Click Here ; to confirm your registration',
  'If clicking this link does not work; copy and paste the link directly into the address bar of your browser. If you are still having problem; simply contact our support team by writing to' => 'If clicking this link does not work; copy and paste the link directly into the address bar of your browser. If you are still having problem; simply contact our support team by writing to',
  'Once you do; you will be able to access to the notification of activity and access of other features that requires a valid email address.' => 'Once you do; you will be able to access to the notification of activity and access of other features that requires a valid email address.',
  'Shipment tracking details - Order ID is #' => 'Shipment tracking details - Order ID is #',
  'Shipment tracking details are' => 'Shipment tracking details are',
  'About' => 'About',
  'This is a static page. You may change the content of this page by updating the file' => 'This is a static page. You may change the content of this page by updating the file',
  'Contact Us' => 'Contact Us',
  'If you have business inquiries or other questions; please fill out the following form to contact us. Thank you.' => 'If you have business inquiries or other questions; please fill out the following form to contact us. Thank you.',
  'Fields with are required' => 'Fields with are required',
  'Please enter the letters as they are shown in the image above.<br/>Letters are not case-sensitive.' => 'Please enter the letters as they are shown in the image above.<br/>Letters are not case-sensitive.',
  'Oops...!' => 'Oops...!',
  'Something went wrong.' => 'Something went wrong.',
  'Go to home' => 'Go to home',
  'Search Only' => 'Search Only',
  'Other Categories' => 'Other Categories',
  'Home' => 'Home',
  'Advertisement' => 'Advertisement',
  'Promotion Details' => 'Promotion Details',
  'Repromote your listing!' => 'Repromote your listing!',
  'Highlight your listing?' => 'Highlight your listing?',
  'allows you to highlight your listing with two different options to reach more number of buyers. You can choose the appropriate option for your listings. Urgent listings gets more leads' => 'allows you to highlight your listing with two different options to reach more number of buyers. You can choose the appropriate option for your listings. Urgent listings gets more leads',
  'Urgent' => 'Urgent',
  'Promote' => 'Promote',
  'To make your ads instantly viewable you can go for Urgent ads; which gets highlighted at the top just for' => 'To make your ads instantly viewable you can go for Urgent ads; which gets highlighted at the top just for',
  'Urgent tag Features' => 'Urgent tag Features',
  'Viewable by all users on desktop and mobile' => 'Viewable by all users on desktop and mobile',
  'Displayed at the top of the page in search results' => 'Displayed at the top of the page in search results',
  'Higher visibility on the website' => 'Higher visibility on the website',
  'Highlight now' => 'Highlight now',
  'Promote your listings to reach more users than normal listings. The promoted listings will be shown at various places to attract the buyers easily' => 'Promote your listings to reach more users than normal listings. The promoted listings will be shown at various places to attract the buyers easily',
  'Days' => 'Days',
  'promote tag Features' => 'promote tag Features',
  'Promote now' => 'Promote now',
  'Changepassword' => 'Changepassword',
  'Back' => 'Back',
  'Notifications' => 'Notifications',
  'Expired' => 'Expired',
  'You may change the content of this page by modifying the file' => 'You may change the content of this page by modifying the file',
  'Login to' => 'Login to',
  'Signup or login to explore the great things available near you' => 'Signup or login to explore the great things available near you',
  'Remember me' => 'Remember me',
  'Forgot Password ?' => 'Forgot Password ?',
  'Not a member yet ?' => 'Not a member yet ?',
  'click here' => 'click here',
  'When my HappySale is Live' => 'When my HappySale is Live',
  'When someone comments on my HappySale' => 'When someone comments on my HappySale',
  'When someone message you on HappySale' => 'When someone message you on HappySale',
  'When you receive offer on HappySale' => 'When you receive offer on HappySale',
  'Enter One Time Password' => 'Enter One Time Password',
  'One Time Password (OTP) has been sent to your mobile' => 'One Time Password (OTP) has been sent to your mobile',
  'please enter the same here to login' => 'please enter the same here to login',
  'Please enter this code' => 'Please enter this code',
  'Verify' => 'Verify',
  'OTP code does not match' => 'OTP code does not match',
  'Phone number' => 'Phone number',
  'Verifications' => 'Verifications',
  'Get verified your phone number to become the trustworthy seller' => 'Get verified your phone number to become the trustworthy seller',
  'Enter your mobile number' => 'Enter your mobile number',
  'Your mobile has been verified' => 'Your mobile has been verified',
  'Change' => 'Change',
  'Facebook' => 'Facebook',
  'Let your facebook users know that you are available here upon verifying your facebook account.' => 'Let your facebook users know that you are available here upon verifying your facebook account.',
  'Your facebook account has been verified' => 'Your facebook account has been verified',
  'Something is wrong. Your facebook account is not verified.' => 'Something is wrong. Your facebook account is not verified.',
  'You have not added any stuff' => 'You have not added any stuff',
  'Sorry…' => 'Sorry…',
  'User is not added any stuff' => 'User is not added any stuff',
  'You have not liked any products.' => 'You have not liked any products.',
  'User is not liked any products' => 'User is not liked any products',
  'Yet no follower are here' => 'Yet no follower are here',
  'Yet no following are here' => 'Yet no following are here',
  'Promotion Type' => 'Promotion Type',
  'Paid Amount' => 'Paid Amount',
  'Up to' => 'Up to',
  'Repromote your list' => 'Repromote your list',
  'Transaction Id' => 'Transaction Id',
  'Status' => 'Status',
  'Yet no product are here' => 'Yet no product are here',
  'Out of 5' => 'Out of 5',
  'Sign Up' => 'Sign Up',
  'Or' => 'Or',
  'Already a member?' => 'Already a member?',
  'Login' => 'Login',
  'Welcome to admin panel' => 'Welcome to admin panel',
  'Items added this week' => 'Items added this week',
  'Promotions added this week' => 'Promotions added this week',
  'Update Categories' => 'Update Categories',
  'Set Top Currency' => 'Set Top Currency',
  'Add Priority' => 'Add Priority',
  'This is the view content for action' => 'This is the view content for action',
  'The action belongs to the controller' => 'The action belongs to the controller',
  'in the' => 'in the',
  'module.' => 'module.',
  'You may customize this page by editing' => 'You may customize this page by editing',
  'Helppages' => 'Helppages',
  'View Helppages #' => 'View Helppages #',
  'Admin Create Products' => 'Admin Create Products',
  'Create Product' => 'Create Product',
  'Productconditions' => 'Productconditions',
  'Update Productconditions' => 'Update Productconditions',
  'Promotions' => 'Promotions',
  'Create Products' => 'Create Products',
  'Update Products' => 'Update Products',
  'Create Sitesettings' => 'Create Sitesettings',
  'Update Sitesettings' => 'Update Sitesettings',
  '1.Select address' => '',
  'Continue' => 'Continue',
  'Nickname' => 'Nickname',
  'Address 1' => 'Address 1',
  'Address 2' => 'Address 2',
  'Zipcode' => 'Zipcode',
  'Visa' => 'Visa',
  'Master Card' => 'Master Card',
  'Discover' => 'Discover',
  'Amex' => 'Amex',
  'Manage Commisions' => 'Manage Commisions',
  'Commission Setup' => 'Commission Setup',
  'View Commissions #' => 'View Commissions #',
  'Add Commission' => 'Add Commission',
  'Invoice No' => 'Invoice No',
  'Order Id' => 'Order Id',
  'View Invoice' => 'View Invoice',
  'Create Orders' => 'Create Orders',
  'Manage Invoices' => 'Manage Invoices',
  'Update Orders' => 'Update Orders',
  'Order No.' => 'Order No.',
  'Order Status' => 'Order Status',
  'Merchant Name' => 'Merchant Name',
  'Sent offer request on' => 'Sent offer request on',
  'My Orders' => 'My Orders',
  'My Sales' => 'My Sales',
  'Qty' => 'Qty',
  'Order on' => 'Order on',
  'Order received' => 'Order received',
  'Order total' => 'Order total',
  'India' => 'India',
  'AD' => 'AD',
  'My orders' => 'My orders',
  'Txn Id' => 'Txn Id',
  'Mark as shipped' => 'Mark as shipped',
  'Mark process' => 'Mark process',
  'Shipping details' => 'Shipping details',
  'Shipment Date' => 'Shipment Date',
  'Shipment Method' => 'Shipment Method',
  'Tracking ID' => 'Tracking ID',
  'Additional Notes' => 'Additional Notes',
  'Address book' => 'Address book',
  'Address details' => 'Address details',
  'Order Details' => 'Order Details',
  'Seller name' => 'Seller name',
  'Payment type' => 'Payment type',
  'Order date' => 'Order date',
  'Shipped date' => 'Shipped date',
  'Delivery confirmed date' => 'Delivery confirmed date',
  'Item amount' => 'Item amount',
  'Received fund' => 'Received fund',
  'Shipping address' => 'Shipping address',
  'Chat with seller' => 'Chat with seller',
  'Shipping detail' => 'Shipping detail',
  'Edit tracking' => 'Edit tracking',
  'Claim' => 'Claim',
  'Chat with buyer' => 'Chat with buyer',
  'Help' => 'Help',
  'Phone no.' => 'Phone no.',
  'Manage Exchanges' => 'Manage Exchanges',
  'You may optionally enter a comparison operator' => 'You may optionally enter a comparison operator',
  'at the beginning of each of your search values to specify how the comparison should be done' => 'at the beginning of each of your search values to specify how the comparison should be done',
  'Create Exchanges' => 'Create Exchanges',
  'Current status' => 'Current status',
  'No Exchanges Found' => 'No Exchanges Found',
  'Update Exchanges' => 'Update Exchanges',
  'Exchange History' => 'Exchange History',
  'View exchange history' => 'View exchange history',
  'location' => 'location',
  'Your stuff successfully posted!' => 'Your stuff successfully posted!',
  'Manage Products' => 'Manage Products',
  'You may optionally enter a comparison operator or at the beginning of each of your search values to specify how the comparison should be done' => 'You may optionally enter a comparison operator or at the beginning of each of your search values to specify how the comparison should be done',
  'Your cart is empty' => 'Your cart is empty',
  'You have' => 'You have',
  'item(s) in your cart' => 'item(s) in your cart',
  'Loading....' => 'Loading....',
  'Previous' => 'Previous',
  'Next' => 'Next',
  'Make an offer' => 'Make an offer',
  'Exchange to buy' => 'Exchange to buy',
  'Edit Item' => 'Edit Item',
  'Comments' => 'Comments',
  'More Info' => 'More Info',
  'Less Info' => 'Less Info',
  'Character left' => 'Character left',
  'Post Comment' => 'Post Comment',
  'View all comments' => 'View all comments',
  'Hide the comments' => 'Hide the comments',
  'About the seller' => 'About the seller',
  'Item(s' => 'Item(s',
  'Follow' => 'Follow',
  'Following(s)' => 'Following(s)',
  'Follower(s)' => 'Follower(s)',
  'Following' => 'Following',
  'Edit Profile' => 'Edit Profile',
  'To' => 'To',
  'You and other person takes the complete responsibility of what is discussed here.' => 'You and other person takes the complete responsibility of what is discussed here.',
  'Send' => 'Send',
  'Message sent' => 'Message sent',
  'Ask price' => 'Ask price',
  'Your offer Price' => 'Your offer Price',
  'Your Offer sent' => 'Your Offer sent',
  'Create Exchange' => 'Create Exchange',
  'Recently Viewed Products' => 'Recently Viewed Products',
  'Ad' => 'Ad',
  'Popular Products' => 'Popular Products',
  'World Wide...' => 'World Wide...',
  'Where do you want to search?' => 'Where do you want to search?',
  'Submit' => 'Submit',
  'or' => 'or',
  'Send Pushnotification' => 'Send Pushnotification',
  'Site Payment Modes' => 'Site Payment Modes',
  'Brain Tree Settings' => 'Brain Tree Settings',
  'Buy Now Management' => 'Buy Now Management',
  'Name should have minimum three characters' => 'Name should have minimum three characters',
  'Special Characters not allowed.' => 'Special Characters not allowed.',
  'Numbers not allowed' => 'Numbers not allowed',
  'Transaction Modes and Configurations' => 'Transaction Modes and Configurations',
  'Exchange Transaction Mode' => 'Exchange Transaction Mode',
  'Buynow Transaction Mode' => 'Buynow Transaction Mode',
  'Note: Select the item location from the dropdown. Avoid entering the location manually.' => 'Note: Select the item location from the dropdown. Avoid entering the location manually.',
  'Report this as inappropriate or broken' => 'Report this as inappropriate or broken',
  'Report this as inappropriate or broken ?' => 'Report this as inappropriate or broken ?',
  'Undo reporting' => 'Undo reporting',
  'Report inappropriate' => 'Report inappropriate',
  'Post comment' => 'Post comment',
  'Paypal ID' => 'Paypal ID',
  'Shipping Cost' => 'Shipping Cost',
  'Minimum one image is required with good resolution' => 'Minimum one image is required with good resolution',
  'Enter your country code and mobile number' => 'Enter your country code and mobile number',
  'Name should have minimum 3 characters' => 'Name should have minimum 3 characters',
  'Username should have minimum 3 characters' => 'Username should have minimum 3 characters',
  'Mark as sold' => 'Mark as sold',
  'Delete Sale' => 'Delete Sale',
  'Delete' => 'Delete',
  'Back to sale' => 'Back to sale',
  'OK' => 'OK',
  'ok' => 'ok',
  'Ok' => 'Ok',

  
);
