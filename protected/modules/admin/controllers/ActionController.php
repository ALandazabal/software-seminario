<?php

class ActionController extends Controller
{
	// Uncomment the following methods and override them if needed
	/*
	public function filters()
	{
		// return the filter configuration for this controller, e.g.:
		return array(
			'inlineFilterName',
			array(
				'class'=>'path.to.FilterClass',
				'propertyName'=>'propertyValue',
			),
		);
	}

	public function actions()
	{
		// return external action classes, e.g.:
		return array(
			'action1'=>'path.to.ActionClass',
			'action2'=>array(
				'class'=>'path.to.AnotherActionClass',
				'propertyName'=>'propertyValue',
			),
		);
	}
	*/
	public $layout = '//layouts/admin';
	
	protected function beforeAction($action)
	{

		if (!parent::beforeAction($action)) {
			return false;
		}
				$user = Yii::app()->adminUser;

	
		if(isset(Yii::app()->adminUser->id)){
						//permisologia

	  $model=Admins::model()->findByPk(Yii::app()->adminUser->id);
		$cont=Yii::app()->controller->id.'controller';
		$controllers=explode(',',$model->permiso);
		$unico=array_unique($controllers);
		$cont_act=array();
		foreach($unico as $controller){	$cont_act[]=explode('-',$controller);	}		
		foreach($cont_act as $contro){			
			if(strtolower($contro[0])==$cont){
				if(strtolower($contro[1])==strtolower($action->id)){

						$ok=true;
				}
			}
		}
		if(!$ok){		echo 'Denied access';			exit();		}
		//FIN permisologia
		}
		if($user->isGuest && Yii::app()->controller->action->id != 'index') {
			$this->redirect(Yii::app()->adminUser->loginUrl);
			return false;
		}elseif(isset(Yii::app()->adminUser->id) && Yii::app()->controller->action->id == 'index'){

			$this->redirect(array('/admin/action/dashboard'));
			return false;
		}
		
		return true;
	}
	
	public function actionIndex($token=null)
	{
		/* if(isset(Yii::app()->adminUser->id))
			$this->redirect(array('/admin/action/dashboard')); */
		$adminToken = Sitesettings::model()->findByAttributes(array('id'=>1));
// 		print_r($adminToken->token);
// 		exit();
		$model=new AdminLoginForm;

		if ($token==$adminToken->token)
		{
		// if it is ajax validation request
		if(isset($_POST['ajax']) && $_POST['ajax']==='adminlogin-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
		
		// collect user input data
		if(isset($_POST['AdminLoginForm']))
		{
			$model->attributes=$_POST['AdminLoginForm'];
		
			// validate user input and redirect to the previous page if valid
			if($model->validate() && $model->login())
				$this->redirect(array('/admin/action/dashboard'));
		}
		}else{
			$this->redirect(array('/site'));
		}
		
		// display the login form
		$this->render('index',array('model'=>$model));
	}
	
	public function actionDashboard(){
		/* if(!isset(Yii::app()->adminUser->id))
			$this->redirect(array('/admin')); */
		
// 		asi obtengo el usuario logueado
// 		$id = Yii::app()->adminUser->id;
// 		$model=Admins::model()->findByPk($id);
// 		print_r($model->email);exit();
		
// 		asi obtengo el usuario logueado
		
		$this->layout = '//layouts/adminwithmenu';
		
				$this->render('dashboard');
		
	}
	
	public function actionSendpushnot(){
// 		print_r($_POST['adminData']);
// 			exit();
		if (isset($_POST['adminData']) && !empty($_POST['adminData'])){
			$message = $_POST['adminData'];
			$date = date('Y-m-d H:i:s');

			$sql = "insert into push_notifications (text,created_at) values (:text,:created_at)";

			$parameters = array(":text"=>$message,
												 "created_at"=>$date);

			Yii::app()->db->createCommand($sql)->execute($parameters);
			
			
			
			
			
			
			
			
			
			/*$notifyTo = $userid;
			if($user_Id == $userid)
				$notifyTo = $senderid;*/
			$notifyMessage = 'sent message';
			Myclass::addLogs("admin", 0, 0, 0, 0, $notifyMessage, 0, $message);
			
			//echo $message; die;
			//$userDetail = Users::model()->findAll(array("condition"=>"userstatus =1 and activationStatus =1"));
			//foreach($userDetail as $userDet){
			//	$userid[] =  $userDet->userId;
		
			//	$criteria = new CDbCriteria;
			//	$criteria->addCondition('user_id = "'.$userid.'"');
			//	$userdevicedet = Userdevices::model()->findAll($criteria);
				$userdevicedet = Userdevices::model()->findAll();
		
		
				if(count($userdevicedet) > 0){
					foreach($userdevicedet as $userdevice){
						$deviceToken = $userdevice->deviceToken;
						$badge = $userdevice->badge;
						$badge +=1;
						$userdevice->badge = $badge;
						$userdevice->deviceToken = $deviceToken;
						$userdevice->save(false);
						if(isset($deviceToken)){
							$messages = $message;
							Myclass::pushnot($deviceToken,$messages,$badge,'admin');
							
						}
					}
				}
			//}
			echo $message;
		}else{
			echo "error";
		}
		
	}

	public function actionCleardevicetoken()
	{
		$type = $_POST['type'];
		if($type == "all")
		{
			Userdevices::model()->deleteAll();
		}
		else if($type == "ios")
		{
			Userdevices::model()->deleteAllByAttributes(array('type'=>'0'));
		}
		else if($type == "android")
		{
			Userdevices::model()->deleteAllByAttributes(array('type'=>'1'));
		}
	}
	
	
		public function actionHistory()
	{
// 			$dataProvider=new CActiveDataProvider('push_notifications');
// 		print_r($dataProvider);
// 		exit();
// 			$this->render('history',array(
// 			'dataProvider'=>$dataProvider,
// 			));
		
		$this->layout = '//layouts/adminwithmenu';
		
		$model=new push_notifications('search');
		$model->unsetAttributes();  // clear any default values
		
		$model->attributes=$_GET['push_notifications'];

		$this->render('history',array(
			'model'=>$model,
		));
	}
	
	
	public function actionBlacklist()
	{
// 			$dataProvider=new CActiveDataProvider('push_notifications');
// 		print_r($dataProvider);
// 		exit();
// 			$this->render('history',array(
// 			'dataProvider'=>$dataProvider,
// 			));
		$this->layout = '//layouts/adminwithmenu';
		
		$model=new Blacklist('search');
		$model->unsetAttributes();  // clear any default values
		
		$model->attributes=$_GET['blacklist'];

		$this->render('history_blacklist',array(
			'model'=>$model,
		));
	}
	

	public function actionStartfileupload()
	{
		$id = $_POST['id'];
			if ( 0 < $_FILES['file1']['error'] ) {
				//echo 'Error: ' . $_FILES['file']['error'] . '<br>';
			}
			$ftmp = $_FILES['file1']['tmp_name'];
			$ftmp1 = $_FILES['file2']['tmp_name'];
			$oname = $_FILES['file1']['name'];
			$fname = $_FILES['file1']['name'];
			$fsize = $_FILES['file1']['size'];
			$ftype = $_FILES['file1']['type'];
			$ext = strrchr($oname, '.');
			$imgpath = dirname(Yii::app()->request->scriptFile).'/certificate/';
			$devfile = "joysaleDevelopment.pem";
			$prodfile = "joysaleProduction.pem";
			chmod($imgpath.$devfile,777);
			chmod($imgpath.$prodfile,777);
			$result = move_uploaded_file($ftmp,$imgpath.$devfile);
			$result1 = move_uploaded_file($ftmp1,$imgpath.$prodfile);
			//echo $usrimg;		
	}
	
	public function actionLogout(){
		
		Yii::app()->adminUser->logout(false); 
		$this->redirect(array('/admin'));
	}
}
