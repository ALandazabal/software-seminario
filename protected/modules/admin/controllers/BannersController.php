<?php

class BannersController extends Controller
{
	/**
	 * @var string the default layout for the views. Defaults to '//layouts/column2', meaning
	 * using two-column layout. See 'protected/views/layouts/column2.php'.
	 */
	public $layout='//layouts/adminwithmenu';

	/**
	 * @return array action filters
	 */
	public function filters()
	{
		return array(
			'accessControl', // perform access control for CRUD operations
			'postOnly + delete', // we only allow deletion via POST request
		);
	}

	/**
	 * Specifies the access control rules.
	 * This method is used by the 'accessControl' filter.
	 * @return array access control rules
	 */
	protected function beforeAction($action)
		{
			if (!parent::beforeAction($action)) {
				return false;
			}
				
			$user = Yii::app()->adminUser;
		if(isset(Yii::app()->adminUser->id)){
						//permisologia

	  $model=Admins::model()->findByPk(Yii::app()->adminUser->id);
		$cont=Yii::app()->controller->id.'controller';
		$controllers=explode(',',$model->permiso);
		$unico=array_unique($controllers);
		$cont_act=array();
		foreach($unico as $controller){	$cont_act[]=explode('-',$controller);	}		
		foreach($cont_act as $contro){			
			if(strtolower($contro[0])==$cont){
				if(strtolower($contro[1])==strtolower($action->id)){

						$ok=true;
				}
			}
		}
		if(!$ok){		echo 'Denied access';			exit();		}
		//FIN permisologia
		}
			if($user->isGuest && Yii::app()->controller->action->id != 'index'&& Yii::app()->controller->action->id != 'resized') {
				$this->redirect(Yii::app()->adminUser->loginUrl);
				return false;
			}elseif(isset(Yii::app()->adminUser->id) && Yii::app()->controller->action->id == 'index'){
				$this->redirect(array('/admin/action/dashboard'));
				return false;
			}
	
			return true;
		}

	/**
	 * Displays a particular model.
	 * @param integer $id the ID of the model to be displayed
	 */
	public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}

	/**
	 * Creates a new model.
	 * If creation is successful, the browser will be redirected to the 'view' page.
	 */
	public function actionCreate()
	{
		$models=new Banners;

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Banners']))
		{
			$models->attributes=$_POST['Banners'];
				/*$bannerUpload = CUploadedFile::getInstance($model,'bannerimage');
				print_r($bannerUpload);
				if(!is_null($bannerUpload)) {

					$model->bannerimage = rand(0000,9999).'_'.$bannerUpload;
				} else {
					$model->bannerimage = "";
				}	
				if($model->save(false)) {
					if(!is_null($bannerUpload)){
						$bannerUpload->saveAs('media/banners/'.str_replace(" ","-",$model->bannerimage));
					}
					}	echo $model->bannerimage;die;*/

			$catImage = CUploadedFile::getInstances($models,'bannerimage');
			$appImage = CUploadedFile::getInstances($models,'appbannerimage');
			list($width,$height) = getimagesize($catImage[0]->tempName);
			list($width1,$height1) = getimagesize($appImage[0]->tempName);
			if($width == "1140" && $height =="325" && $width1 == "1024" && $height1 == "500")
			{
				if(!empty($catImage)) {
					$imageName = explode(".",$catImage[0]->name);
					$models->bannerimage = rand(000,9999).'-'.Myclass::productSlug($imageName[0]).'.'.$catImage[0]->extensionName;
				}
					if(!empty($catImage)) {  
						$catImage[0]->saveAs('media/banners/'. $models->bannerimage);
					}

				$appImage = CUploadedFile::getInstances($models,'appbannerimage');
				if(!empty($appImage)) {
					$imageName = explode(".",$appImage[0]->name);
					$models->appbannerimage = rand(000,9999).'-'.Myclass::productSlug($imageName[0]).'.'.$appImage[0]->extensionName;
				}
					if(!empty($appImage)) {  
						$appImage[0]->saveAs('media/banners/'. $models->appbannerimage);
					}				

				if($models->save())
					$this->redirect(array('view','id'=>$models->id));
			}
			else
			{
				Yii::app()->user->setFlash('success','Please upload the image with the specified size');
				$this->redirect(array('create'));
			}
		}

		$this->render('create',array(
			'model'=>$models,
		));
	}

	/**
	 * Updates a particular model.
	 * If update is successful, the browser will be redirected to the 'view' page.
	 * @param integer $id the ID of the model to be updated
	 */
	public function actionUpdate($id)
	{
		$models=$this->loadModel($id);

		// Uncomment the following line if AJAX validation is needed
		// $this->performAjaxValidation($model);

		if(isset($_POST['Banners']))
		{
			$models->attributes=$_POST['Banners'];
				/*$bannerUpload = CUploadedFile::getInstance($model,'bannerimage');
				print_r($bannerUpload);
				if(!is_null($bannerUpload)) {

					$model->bannerimage = rand(0000,9999).'_'.$bannerUpload;
				} else {
					$model->bannerimage = "";
				}	
				if($model->save(false)) {
					if(!is_null($bannerUpload)){
						$bannerUpload->saveAs('media/banners/'.str_replace(" ","-",$model->bannerimage));
					}
					}	echo $model->bannerimage;die;*/

			$catImage = CUploadedFile::getInstances($models,'bannerimage');
			$appImage = CUploadedFile::getInstances($models,'appbannerimage');
			list($width,$height) = getimagesize($catImage[0]->tempName);
			list($width1,$height1) = getimagesize($appImage[0]->tempName);
			if(!empty($catImage))
			{
				if($width == "1140" && $height =="325")
				{
					if(!empty($catImage)) {
						$imageName = explode(".",$catImage[0]->name);
						$models->bannerimage = rand(000,9999).'-'.Myclass::productSlug($imageName[0]).'.'.$catImage[0]->extensionName;
					}
						if(!empty($catImage)) {  
							$catImage[0]->saveAs('media/banners/'. $models->bannerimage);
						}
				}
				else
				{
					Yii::app()->user->setFlash('success','Please upload the image with the specified size');
					$this->redirect(array('update','id'=>$models->id));
				}
			}
			else if(!empty($appImage))
			{
				if($width1 == "1024" && $height1 == "500" || $width1 == "2048" && $height1 == "1000")
				{
					$appImage = CUploadedFile::getInstances($models,'appbannerimage');
					if(!empty($appImage)) {
						$imageName = explode(".",$appImage[0]->name);
						$models->appbannerimage = rand(000,9999).'-'.Myclass::productSlug($imageName[0]).'.'.$appImage[0]->extensionName;
					}
						if(!empty($appImage)) {  
							$appImage[0]->saveAs('media/banners/'. $models->appbannerimage);
						}				

				}
				else
				{
					Yii::app()->user->setFlash('success','Please upload the image with the specified size');
					$this->redirect(array('update','id'=>$models->id));
				}
			}
			if($models->save(false))
					$this->redirect(array('view','id'=>$models->id));
		}

		$this->render('update',array(
			'model'=>$models,
		));
	}

	/**
	 * Deletes a particular model.
	 * If deletion is successful, the browser will be redirected to the 'admin' page.
	 * @param integer $id the ID of the model to be deleted
	 */
	public function actionDelete($id)
	{
		$this->loadModel($id)->delete();

		// if AJAX request (triggered by deletion via admin grid view), we should not redirect the browser
		if(!isset($_GET['ajax']))
			$this->redirect(isset($_POST['returnUrl']) ? $_POST['returnUrl'] : array('admin'));
	}

	/**
	 * Lists all models.
	 */
	public function actionIndex()
	{
		$siteSettings = Sitesettings::model()->find();
		$dataProvider=new CActiveDataProvider('Banners');
		$this->render('index',array(
			'dataProvider'=>$dataProvider,
			'sitesettings' => $sitesettings,
		));
	}

	public function actionBannerenable()
	{
		$enablestatus = $_POST['enablestatus'];
		$sitesettings = Sitesettings::model()->findByPk(1);
		$sitesettings->bannerstatus = $enablestatus;
		$sitesettings->save(false);
	}

	/**
	 * Manages all models.
	 */
	public function actionAdmin()
	{
		$sitesettings = Sitesettings::model()->find();
		$model=new Banners('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Banners']))
			$model->attributes=$_GET['Banners'];

		$this->render('admin',array(
			'model'=>$model,
			'sitesettings' => $sitesettings,
		));
	}

	public function actionCheckimage()
	{
			if ( 0 < $_FILES['file']['error'] ) {
				//echo 'Error: ' . $_FILES['file']['error'] . '<br>';
			}
			$ftmp = $_FILES['file']['tmp_name'];
			$oname = $_FILES['file']['name'];
			$fname = $_FILES['file']['name'];
			$fsize = $_FILES['file']['size'];
			$ftype = $_FILES['file']['type'];
			list($width,$height) = getimagesize($ftmp);
			if($width == "1140" && $height == "325")
			{
				echo "success";
			}
			else
			{
				echo "error";
			}
	}

	/**
	 * Returns the data model based on the primary key given in the GET variable.
	 * If the data model is not found, an HTTP exception will be raised.
	 * @param integer $id the ID of the model to be loaded
	 * @return Banners the loaded model
	 * @throws CHttpException
	 */
	public function loadModel($id)
	{
		$model=Banners::model()->findByPk($id);
		if($model===null)
			throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}

	/**
	 * Performs the AJAX validation.
	 * @param Banners $model the model to be validated
	 */
	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='banners-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	}
}
