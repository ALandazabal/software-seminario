<?php 	
class AdminsController extends Controller
{
	public $layout='//layouts/adminwithmenu';
	protected function beforeAction($action)
		{
			if (!parent::beforeAction($action)) {
				return false;
			}
				
			$user = Yii::app()->adminUser;
		if(isset(Yii::app()->adminUser->id)){
						//permisologia

	  $model=Admins::model()->findByPk(Yii::app()->adminUser->id);
		$cont=Yii::app()->controller->id.'controller';
		

		$controllers=explode(',',$model->permiso);
		$unico=array_unique($controllers);
		$cont_act=array();
		foreach($unico as $controller){	$cont_act[]=explode('-',$controller);	}		
		foreach($cont_act as $contro){			
		
			if(strtolower($contro[0])==$cont)
			{
				// echo strtolower($contro[0]);

				if(strtolower($contro[1])==strtolower($action->id))
				{
					
						$ok=true;
						// exit();
				}
			}
		}

		if(!$ok){		echo 'Denied access';			exit();		}
		//FIN permisologia
		}
			if($user->isGuest && Yii::app()->controller->action->id != 'index'&& Yii::app()->controller->action->id != 'resized') {
				$this->redirect(Yii::app()->adminUser->loginUrl);
				return false;
			}elseif(isset(Yii::app()->adminUser->id) && Yii::app()->controller->action->id == 'index'){
				$this->redirect(array('/admin/action/dashboard'));
				return false;
			}
	
			return true;
		}
public function actionAdmin()
	{
	 
	//	$id = Yii::app()->adminUser->id;
	//	$modeladmin=Admins::model()->findByPk($id);
// 		print_r($modeladmin->type);exit();
// 		prueba de usuario administrador 
		//if($modeladmin->type == "admin1"){
			//print_r("Usuario no tiene los permisos"); exit();
		//}else{
// 		$controllers = Yii::app()->metadata->getControllers('admin');

				$model=new Admins('search');
		$model->unsetAttributes();  // clear any default values
		if(isset($_GET['Admins']))
		$model->attributes=$_GET['Admins'];

		$this->render('admin',array(
			'model'=>$model,
		));
		//}
		
	
		
		
	}
	
		public function actionView($id)
	{
		$this->render('view',array(
			'model'=>$this->loadModel($id),
		));
	}
	
		public function loadModel($id)
	{
		$model=Admins::model()->findByPk($id);
		if($model===null)
		throw new CHttpException(404,'The requested page does not exist.');
		return $model;
	}
	
	
		public function actionCreate()
	{
		$model=new Admins('create');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);

		if(isset($_POST['Admins']))
		{
			$model->attributes=$_POST['Admins'];
			$password = $_POST['Admins']['password'];
			$model->type = 'admin';
			
				if($model->validate()) {
				$model->password = base64_encode($password);
				$model->save(false);

				Yii::app()->user->setFlash('success',Yii::t('admin','User Admins Created Successfully'));
				$this->redirect(array('admin'));
			}
			
			}
				$this->render('create',array(
			'model'=>$model,
		));
		}
	
  	public function actionUpdate($id)
	{
		

		$model=$this->loadModel($id);
// 		print_r($model);
// 		print_r($model->permiso);
		$controllers=explode(',',$model->permiso);
// 		print_r($controllers);
	$unico=array_unique($controllers);
		
		$cont_act=array();
		foreach($unico as $controller){
			$cont_act[]=explode('-',$controller);
			
			
		}
		
		
// 		print_r($cont_act);
// 		exit();
		$model->setScenario('update');

		// Uncomment the following line if AJAX validation is needed
		$this->performAjaxValidation($model);
		$model->password = base64_decode($model->password);
		if(isset($_POST['Admins']))
		{
			
			$model->attributes=$_POST['Admins'];
			$model->password = base64_encode($_POST['Admins']['password']);

			if($model->save()){
				Yii::app()->user->setFlash('success',Yii::t('admin','User Admins Details Updated Successfully'));
// 				$this->redirect(array('admin'));
			}
		}
		 $controllersWithActions = Yii::app()->metadata->getControllersActions('admin');
// 		var_dump($controllersWithActions); #Get list of application controllers
		$this->render('update',array(
			'model'=>$model,
			'controladores'=>$controllersWithActions,
			'permisos'=>$cont_act
		));
	}
  	public function actionUpdatepermisos()
	{
	

		if(isset($_POST))
		{
			$model=$this->loadModel($_POST['id']);
			$model->setScenario('update');
			$model->attributes=$_POST;

			if($model->save()){
				Yii::app()->user->setFlash('success',Yii::t('admin','User Admins Details Updated Successfully'));
// 				$this->redirect(array('admin'));
			}
		}
		 $controllersWithActions = Yii::app()->metadata->getControllersActions('admin');
// 		var_dump($controllersWithActions); #Get list of application controllers
			$controllers=explode(',',$model->permiso);
			$unico=array_unique($controllers);
// 		print_r($controllers);
		$cont_act=array();
		foreach($unico as $controller){
			$cont_act[]=explode('-',$controller);
			
			
		}
		$this->render('update',array(
			'model'=>$model,
			'controladores'=>$controllersWithActions,
						'permisos'=>$cont_act

		));
	}
  
  
 	protected function performAjaxValidation($model)
	{
		if(isset($_POST['ajax']) && $_POST['ajax']==='admins-form')
		{
			echo CActiveForm::validate($model);
			Yii::app()->end();
		}
	} 
  
  
}
?>