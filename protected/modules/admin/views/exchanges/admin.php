<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>



<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
			<?php echo Yii::t('admin','Manage').' '.Yii::t('admin','Exchanges'); ?>
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				<?php echo Yii::t('admin','Exchanges').' '.Yii::t('admin','List'); ?>
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
				<?php 
           $exchangesname = Myclass::getExchangesData();
					
						
						
//           print_r($exchangesname);
          ?>
          

                   
                      <table id="table_id" class="display">
                      <thead>
                          <tr>
                              <th>id</th>
                              <th>requestFrom</th>
                            <th>requestTo</th>
                            <th>mainProductId</th>
                            <th>exchangeProductId</th>
                            <th>status</th>
<!--                             <th>date</th>
                            <th>slug</th>
                            <th>blockExchange</th>
                            <th>exchangeHistory</th>
                            <th>reviewFlagSender</th>
                            <th>reviewFlagReceiver</th> -->
                          </tr>
                      </thead>
                      <tbody>
                        <?php foreach($exchangesname as $exchange): ?>
												<?php 
														$username_requestfrom = Myclass::getUserName($exchange->requestFrom);
														$username_requestto = Myclass::getUserName($exchange->requestTo);
												$main_product_id = Myclass::getProductsName($exchange->mainProductId);
												$exchange_product_id = Myclass::getProductsName($exchange->exchangeProductId);
												
												?>
                          <tr>
                            <td><?php echo $exchange->id; ?></td>
														<td><?php echo $username_requestfrom; ?></td>
                            <td><?php echo $username_requestto; ?></td>
                            <td><?php echo $main_product_id; ?></td>
                            <td><?php echo $exchange_product_id; ?></td>
													<?php 
														if($exchange->status==1){
															?><td><?php echo "ACCEPT"; ?></td><?php
														}else if($exchange->status==2)
														{
															?><td><?php echo "DECLINE"; ?></td><?php
														}else if($exchange->status==3)
														{
															?><td><?php echo "CANCEL"; ?></td><?php
														}else if($exchange->status==4){
															?><td><?php echo "SUCCESS"; ?></td><?php
														}else if($exchange->status==5){
															?><td><?php echo "FAILED"; ?></td><?php
														}else if($exchange->status==6){
															?><td><?php echo "SOLDOUT"; ?></td><?php
														}else{
															?><td><?php echo "NULL"; ?></td><?php
														}
														?>
<!--                             <td><?php echo $exchange->date; ?></td>
                            <td><?php echo $exchange->slug; ?></td>
                            <td><?php echo $exchange->blockExchange; ?></td>
                            <td><?php echo $exchange->exchangeHistory; ?></td>
                            <td><?php echo $exchange->reviewFlagSender; ?></td>
                            <td><?php echo $exchange->reviewFlagReceiver; ?></td> -->
                          </tr>
                       <?php endforeach; ?>
                      </tbody>
                  </table>
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<script>
  $(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>

