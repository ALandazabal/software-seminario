<link rel="stylesheet" type="text/css" href="//cdn.datatables.net/1.10.13/css/jquery.dataTables.css">
<script type="text/javascript" charset="utf8" src="//cdn.datatables.net/1.10.13/js/jquery.dataTables.js"></script>



<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header">
			<?php echo Yii::t('admin','History').' '.Yii::t('admin','Notifications'); ?>
			</h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
				<?php echo Yii::t('admin','Notifications').' '.Yii::t('admin','List'); ?>
				</div>
				<!-- /.panel-heading -->
				<div class="panel-body">
				<?php 
           $listnotifications = Myclass::getPushnotifications();
          ?>
   
                      <table id="table_id" class="display">
													<thead>
															<tr>
																	<th>Id</th>
																	<th>Notifications Text</th>
																	<th>Created At</th>

															</tr>
													</thead>
													<tbody>
														<?php foreach($listnotifications as $notifications): ?>

															<tr>
																<td><?php echo $notifications->id; ?></td>
																<td><?php echo $notifications->text; ?></td>
																<td><?php echo $notifications->created_at; ?></td>

															</tr>
													 <?php endforeach; ?>
													</tbody>
                  </table>
				</div>
				<!-- /.panel-body -->
			</div>
			<!-- /.panel -->
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<!-- /.row -->
</div>
<script>
  $(document).ready( function () {
    $('#table_id').DataTable();
} );
</script>