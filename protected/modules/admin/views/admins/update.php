<style>
	.content-page{
		overflow-y: scroll;
	}
</style>
<div id="page-wrapper">
	<div class="row">
		<div class="col-lg-12">
			<h1 class="page-header"><?php echo Yii::t('admin','Update').' '.Yii::t('admin','Users Admins'); ?></h1>
		</div>
		<!-- /.col-lg-12 -->
	</div>
	<div class="row">
		<div class="col-lg-12">
			<div class="panel panel-default">
				<div class="panel-heading">
					<?php echo Yii::t('admin','Update').' '.Yii::t('admin','User Admins'); ?>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="col-lg-6">
	<?php $this->renderPartial('_form', array('model'=>$model, 'type'=>"update")); ?>

				
					
						</div>
					</div><!-- /.row (nested) -->
					<div class="row">
						<h1>Permissions <small>Manage permissions... Controllers / Actions</small></h1>
						<?php foreach ($controladores as $controlador ): ?>
				
						<div class="col-md-4" style="padding:20px;">
														 <div class="button-group">
																			<button type="button" class="btn btn-default btn-sm dropdown-toggle" data-toggle="dropdown"><?=$controlador['name']?> <span class="caret"></span></button>
															<ul class="dropdown-menu">
						<?php foreach ($controlador['actions'] as $action ): ?>
            <?php foreach ($permisos as $permiso): ?>
                <?php if ($permiso[0]==strtolower($controlador['name'])): ?>
                    <?php if ($permiso[1]==strtolower($action)): $mostro=true;?>
								<li><a href="#" class="small" data-value="<?=strtolower($controlador['name']).'-'.strtolower($action)?>" tabIndex="-1"><input type="checkbox" checked="true"/>&nbsp;<?=$action?></a></li>


                    <?php  endif ?>

                <?php endif ?>

            <?php endforeach ?>
																<?php if (!$mostro): ?>
																           <li><a href="#" class="small" data-value="<?=strtolower($controlador['name']).'-'.strtolower($action)?>" tabIndex="-1"><input type="checkbox"/>&nbsp;<?=$action?></a></li>
	                <?php  endif ?>

	          <?php $mostro=false; endforeach ?>

																	
															</ul>
																</div>
							</div>
					<?php endforeach ?>		
					
					</div>
					<div class="row">
						 <form  method="post" action="<?php echo $baseUrl."/admin/admins/updatepermisos"; ?>">
						<input type="hidden"	name="permiso" id="permisos" value="<?=$model->permiso?>">			
					 <input type="hidden"	name="id" value="<?= $model->id?>">

							 <button type="submit" class="btn btn-success">Save</button>
						</form>
					</div>
				</div><!-- /.panel-body -->
			</div><!-- /.panel -->
		</div><!-- /.col-lg-12 -->
	</div><!-- /.row -->
</div><!-- /#page-wrapper -->
<script>
$( document ).ready(function() {
	var viejo= $('#permisos').val();
	if(viejo!=""){
		var options=$('#permisos').val().split(",");
	}else
	{
		  var options = [];

	}
console.log(options);
$( '.dropdown-menu a' ).on( 'click', function( event ) {

   var $target = $( event.currentTarget ),
       val = $target.attr( 'data-value' ),
       $inp = $target.find( 'input' ),
       idx;

   if ( ( idx = options.indexOf( val ) ) > -1 ) {
      options.splice( idx, 1 );
      setTimeout( function() { $inp.prop( 'checked', false ) }, 0);
   } else {
      options.push( val );
      setTimeout( function() { $inp.prop( 'checked', true ) }, 0);
   }

   $( event.target ).blur();
	$('#permisos').val();
      $('#permisos').val(options.toString()); 
   console.log( options );
   return false;
});
});

</script>