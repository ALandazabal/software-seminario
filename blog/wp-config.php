<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'admin_blog');

/** MySQL database username */
define('DB_USER', 'admin_blog');

/** MySQL database password */
define('DB_PASSWORD', 'JlLFL9m2qi');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'Z}NPT%:Q&=qC[?[pUohrp`_Aen]&&(}9}CWSa1IG~@5[)q+).Ar2rpmjrcuedN?Y');
define('SECURE_AUTH_KEY',  'i~zG%9u1LnHj>}@n6cp/eXM@i()|lZ5fZlZlV<23#hRn_nAf# m9u/:C7,g_;W,q');
define('LOGGED_IN_KEY',    'm$V}l2tEbA=dKx?FWH%8tKrH+RP9>i30OUk}3&uhh|DoBP^&AO;2] :WQ,UFO]2R');
define('NONCE_KEY',        'L.nL/xF6%jfj*5!Z*n$7iQUF+<[#>7];VXn/pE(hw(@&C*$j=OFOSeb|CAWuYs=c');
define('AUTH_SALT',        'tbBTavEcTJ1pcfwO0<*,{;wJ1[M!2cr^*~FL2M[M=8_D`4nyq{*B4f&U&LThf2m!');
define('SECURE_AUTH_SALT', '}+m=CyM84TV`EF[30ZrW,2or+W9U||g)v*?+7l|Cabl^I9u+Cc ,u/j*t:)gm9ni');
define('LOGGED_IN_SALT',   '_n6GumE*siZ/?zfE:AY@-?tdT:#%c?>L:u[[G#p3@mVw>B|bF#hxkS@s:]@)rP.y');
define('NONCE_SALT',       '8{{3Nmg(?3_n:j>C3>/v,H]U/Fw/fOP|&:NBDkO:(RB2<iyh~Y*vc-!QfhKG`cK.');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
